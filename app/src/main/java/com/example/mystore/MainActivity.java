package com.example.mystore;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.mystore.adapter.LowStockAdapter;
import com.example.mystore.db.DbHelper;
import com.example.mystore.model.Products;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    Button b1,b2,b3,b4,btncus;
    Spinner spinnerdash;
    TextView tvsaleamnt,tvsalepercentage,tvsaleqty,tvsaleyr,tvsaleamntlastyr,
                tvpurpercentage,tvpurchaseamnt,tvpurqty,tvpurlastyr,tvpuramntlastyr,
                tvstockproname,tvstocktotalpro,tvmsproname,tvmsproqty,tvlwstockproname,tvlwstockqty;
     EditText atvname;
     String selectsortby;
     RecyclerView lowstkrcview;
     DbHelper myDb;
     List<Products> lwstklist;
     int monthsale,weeksale,yearsale,pvmonthsale,pvweeksale,pvyearsale;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        b1 = findViewById(R.id.button1);
        b2 = findViewById(R.id.button2);
        b3 = findViewById(R.id.btnsale);
        b4 = findViewById(R.id.btnreport);
        btncus = findViewById(R.id.btncus);
        tvsaleamnt = findViewById(R.id.tvsaleamnt);
        tvsalepercentage = findViewById(R.id.tvsalepercentage);
       // tvsaleqty = findViewById(R.id.tvsaleqty);
        tvsaleyr =  findViewById(R.id.tvsaleyr);
        tvsaleamntlastyr= findViewById(R.id.tvsaleamntlastyr);
        tvpuramntlastyr = findViewById(R.id.tvpuramntlastyr);
        tvpurpercentage = findViewById(R.id.tvpurpercentage);
        tvpurchaseamnt = findViewById(R.id.tvpurchaseamnt);
        tvpurqty = findViewById(R.id.tvpurqty);
        tvpurlastyr = findViewById(R.id.tvpurlastyr);
        //tvstockproname = findViewById(R.id.tvstockproname);
       // tvstocktotalpro = findViewById(R.id.tvstocktotalpro);
        tvmsproname = findViewById(R.id.tvmsproname);
        tvmsproqty = findViewById(R.id.tvmsproqty);
        myDb = new DbHelper(this);

        atvname =  findViewById(R.id.atvname);
        lowstkrcview = findViewById(R.id.lowstockrcview);

        List <String> sortby = new ArrayList<>();
        sortby.add("Week");
        sortby.add("Month");
        sortby.add("Year");

        spinnerdash = (Spinner) findViewById(R.id.spinnerdash);
        ArrayAdapter<String> sort = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, sortby);
        sort.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerdash.setAdapter(sort);
        selectsortby  = spinnerdash.getSelectedItem().toString();

        monthsale = myDb.getsalebymonth();
        yearsale = myDb.getsalebyyear();
        weeksale = myDb.getsalebyweek();
        pvweeksale = myDb.getsalebypreviousweek();
        pvmonthsale = myDb.getsalebypreviousmonth();
        tvsaleamnt.setText(String.valueOf(weeksale));
        tvsaleamntlastyr.setText(String.valueOf(pvweeksale));
        tvsaleyr.setText("Previous Week");

     //   tvsalepercentage.setText(String.valueOf((pvweeksale-weeksale)/weeksale*100));

        spinnerdash.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                selectsortby  = spinnerdash.getSelectedItem().toString();

                switch (selectsortby) {
                    case "Week":
                        weeksale = myDb.getsalebyweek();
                        tvsaleamnt.setText(String.valueOf(weeksale));
                        tvsaleamntlastyr.setText(String.valueOf(pvweeksale));
                        tvsaleyr.setText("Previous Week");
                     //   tvsalepercentage.setText(String.valueOf((weeksale*100)/pvweeksale));
                        break;
                    case "Month":
                        monthsale = myDb.getsalebymonth();
                        pvmonthsale = myDb.getsalebypreviousmonth();
                        tvsaleamnt.setText(String.valueOf(monthsale));
                        tvsaleamntlastyr.setText(String.valueOf(pvmonthsale));
                        tvsaleyr.setText("Previous Month");
                        break;
                    case "Year":
                        yearsale = myDb.getsalebyyear();
                        pvyearsale = myDb.getsalebypreviousyear();
                        tvsaleamnt.setText(String.valueOf(yearsale));
                        tvsaleamntlastyr.setText(String.valueOf(pvyearsale));
                        tvsaleyr.setText("Previous Year");
                        break;

                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });

        lwstklist = new ArrayList<>();

        lwstklist = myDb.getLowstkock();

        final int time = 1000;
        final List<Products> flwlist = new ArrayList<>();

        for(Products item : lwstklist){
            if(Integer.parseInt( item.getQuantity()) < 2){
                flwlist.add(item);
            }
        }



         final LowStockAdapter  madapter1 = new LowStockAdapter(this,flwlist);


        lowstkrcview.setAdapter(madapter1);




        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        lowstkrcview.setLayoutManager(linearLayoutManager);

        //The LinearSnapHelper will snap the center of the target child view to the center of the attached RecyclerView , it's optional if you want , you can use it
        final LinearSnapHelper linearSnapHelper = new LinearSnapHelper();
        linearSnapHelper.attachToRecyclerView(lowstkrcview);

        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {

                if (linearLayoutManager.findLastCompletelyVisibleItemPosition() < (madapter1.getItemCount() - 1)) {

                    linearLayoutManager.smoothScrollToPosition(lowstkrcview, new RecyclerView.State(), linearLayoutManager.findLastCompletelyVisibleItemPosition() + 1);
                }

                else if (linearLayoutManager.findLastCompletelyVisibleItemPosition() == (madapter1.getItemCount() - 1)) {

                    linearLayoutManager.smoothScrollToPosition(lowstkrcview, new RecyclerView.State(), 0);
                }
            }
        }, 0, time);







        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,AddproductsActivity.class);
                startActivity(intent);

            }
        });
        btncus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,CustomerActivity.class);
                startActivity(intent);

            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,ProductsActivity.class);
                startActivity(intent);

            }
        });

        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,SaleActivity.class);
                startActivity(intent);

            }
        });
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,ReportActivity.class);
                startActivity(intent);

            }
        });
    }




}