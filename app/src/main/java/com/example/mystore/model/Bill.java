package com.example.mystore.model;

public class Bill {

    int productid,ordersid,orderitemid;
    String productname,qty,total,date,rate;

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public Bill() {
    }

    public Bill(int productid, int orderid, int orderitemid, String date, String productname, String qty, String total) {
        this.productid = productid;
        this.ordersid = orderid;
        this.orderitemid = orderitemid;
        this.productname = productname;
        this.qty = qty;
        this.total = total;
        this.date= date;

    }


    public Bill(int orderid, String date, String total, String productname) {
        this.ordersid = orderid;
        this.total = total;
        this.date = date;
        this.productname = productname;

    }
    public Bill(int orderid,String total, String date ) {
        this.ordersid = orderid;
        this.total = total;
        this.date = date;


    }
    public Bill( String date, String total,String productname) {

        this.total = total;
        this.date = date;
        this.productname = productname;

    }

    public Bill(int ordersid, String date,String total, String qty, String rate, String productname) {
        this.ordersid = ordersid;
        this.rate = rate;
        this.productname = productname;
        this.qty = qty;
        this.total = total;
        this.date= date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getProductid() {
        return productid;
    }

    public void setProductid(int productid) {
        this.productid = productid;
    }

    public int getOrderid() {
        return ordersid;
    }

    public void setOrderid(int orderid) {
        this.ordersid = orderid;
    }

    public int getOrderitemid() {
        return orderitemid;
    }

    public void setOrderitemid(int orderitemid) {
        this.orderitemid = orderitemid;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
