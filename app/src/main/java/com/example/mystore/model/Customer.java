package com.example.mystore.model;

public class Customer {
    int Cid;
    String Customername,mobile,email,address;

    public Customer() {
    }

    public Customer(int cid, String mobile, String customername, String email, String address) {
        Cid = cid;
        Customername = customername;
        this.mobile = mobile;
        this.email = email;
        this.address = address;
    }

    public int getCid() {
        return Cid;
    }

    public void setCid(int cid) {
        Cid = cid;
    }

    public String getCustomername() {
        return Customername;
    }

    public void setCustomername(String customername) {
        Customername = customername;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
