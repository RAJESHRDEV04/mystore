package com.example.mystore.model;

public class Products {
    String category,brandname;
    int categoryid,productid;
String productname,productdetails,barcode,productimage,quantity,buyprice,sellprice,status,date;


    public Products(int productid, int categoryid,String productname, String productdetails,String buyprice, String sellprice, String quantity,String barcode, String productimage,  String status) {

        this.categoryid = categoryid;
        this.productid = productid;
        this.productname = productname;
        this.productdetails = productdetails;
        this.barcode=  barcode;
        this.productimage = productimage;
        this.quantity = quantity;
        this.buyprice = buyprice;
        this.sellprice = sellprice;
        this.status = status;
    }
    public Products(int productid, String category,String brandname ,String productname, String productdetails,String buyprice, String sellprice, String quantity,String barcode, String productimage, String date, String status) {
       this.brandname = brandname;
        this.category = category;
        this.productid = productid;
        this.productname = productname;
        this.productdetails = productdetails;
        this.barcode=  barcode;
        this.productimage = productimage;
        this.quantity = quantity;
        this.buyprice = buyprice;
        this.sellprice = sellprice;
        this.status = status;
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBarcode() {
        return barcode;
    }

    public String getBrandname() {
        return brandname;
    }

    public void setBrandname(String brandname) {
        this.brandname = brandname;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(int categoryid) {
        this.categoryid = categoryid;
    }

    public int getProductid() {
        return productid;
    }

    public void setProductid(int productid) {
        this.productid = productid;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getProductdetails() {
        return productdetails;
    }

    public void setProductdetails(String productdetails) {
        this.productdetails = productdetails;
    }

    public String getProductimage() {
        return productimage;
    }

    public void setProductimage(String productimage) {
        this.productimage = productimage;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getBuyprice() {
        return buyprice;
    }

    public void setBuyprice(String buyprice) {
        this.buyprice = buyprice;
    }

    public String getSellprice() {
        return sellprice;
    }

    public void setSellprice(String sellprice) {
        this.sellprice = sellprice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
