package com.example.mystore.db;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;


import com.example.mystore.model.Bill;
import com.example.mystore.model.Customer;
import com.example.mystore.model.Products;

import java.util.ArrayList;
import java.util.List;

public class DbHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "store.db";


    public static final String TABLE_1 = "Categories";
    public static final String TABLE_2 = "Orders";
    public static final String TABLE_3 = "Orderitem";
    public static final String TABLE_4 = "Product";
    public static final String TABLE_5 = "Stock";
    public static final String TABLE_6 = "Users";
    public static final String TABLE_7 = "Customer";
    public static final String TABLE_8 = "OrderCustomer";
    public DbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 1);
    }




    @Override
    public void onCreate(SQLiteDatabase db) {

        String table1 = "CREATE TABLE " + TABLE_1 + "(CATEGORIESID INTEGER PRIMARY KEY AUTOINCREMENT,CATEGORIESNAME TEXT,BRANDNAME TEXT)";
        String table2 = "CREATE TABLE " + TABLE_2 + "(ORDERSID INTEGER PRIMARY KEY AUTOINCREMENT, GRANDTOTAL TEXT,GSTN TEXT,ORDERDATE TEXT,ORDERSTATUS TEXT,PAID TEXT,PAYMENTPLACE TEXT,PAYMENTSTATUS TEXT,PAYMENTTYPE TEXT,SUBTOTAL TEXT,TOTALAMOUNT TEXT,ORDERCUSTOMERID INTEGER,VAT TEXT,FOREIGN KEY(ORDERCUSTOMERID) REFERENCES TABLE_8(ORDERCUSTOMERID))";
        String table3 = "CREATE TABLE " + TABLE_3 + "(ORDERITEMID INTEGER PRIMARY KEY AUTOINCREMENT,ORDERSID INTEGER,ORDERITEMSTATUS INTEGER,PRODUCTNAME TEXT,QUANTITY TEXT,RATE TEXT,TOTAL TEXT,FOREIGN KEY(ORDERSID) REFERENCES TABLE_2(ORDERSID))";
        String table4 = "CREATE TABLE " + TABLE_4 + "(PRODUCTID INTEGER PRIMARY KEY AUTOINCREMENT,CATEGORIESID INTEGER,PRODUCTNAME TEXT,PRODUCTDETAILS TEXT,BUYPRICE TEXT,SELLPRICE TEXT,STOCKID INTEGER, BARCODE TEXT,PRODUCTIMAGE TEXT,DATE TEXT, STATUS TEXT,FOREIGN KEY(CATEGORIESID) REFERENCES TABLE_1(CATEGORIESID),FOREIGN KEY(STOCKID) REFERENCES TABLE_5(STOCKID))";
        String table5 = "CREATE TABLE " + TABLE_5 + "(STOCKID INTEGER PRIMARY KEY AUTOINCREMENT,PRODUCTID INTEGER,SDATE TEXT,SQUANTITY TEXT, QUANTITY TEXT,FOREIGN KEY(PRODUCTID) REFERENCES TABLE_4(PRODUCTID))";
        String table6 = "CREATE TABLE " + TABLE_6 + "(USERID INTEGER PRIMARY KEY AUTOINCREMENT,USERNAME TEXT,PASSWORD TEXT,EMAIL TEXT)";
        String table7 = "CREATE TABLE " + TABLE_7 + "(CUSTOMERID INTEGER PRIMARY KEY AUTOINCREMENT,MOBILE TEXT,NAME TEXT,EMAIL TEXT,ADDRESS TEXT)";
        String table8 = "CREATE TABLE " + TABLE_8 + "(ORDERCUSTOMERID INTEGER PRIMARY KEY AUTOINCREMENT,ORDERSID INTEGER,MOBILE TEXT,NAME TEXT,EMAIL TEXT,ADDRESS TEXT,FOREIGN KEY(ORDERSID) REFERENCES TABLE_2(ORDERSID))";

        db.execSQL(table1);
        db.execSQL(table2);
        db.execSQL(table3);
        db.execSQL(table4);
        db.execSQL(table5);
        db.execSQL(table6);
        db.execSQL(table7);
        db.execSQL(table8);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_1);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_2);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_3);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_4);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_5);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_6);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_7);
        onCreate(db);

    }


    public  boolean insertCustomer(String mobile,String cname,String email,String address){
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("MOBILE",mobile);

        cv.put("NAME",cname);

        cv.put("EMAIL",email);
        cv.put("ADDRESS",address);
        long result = db.insert(TABLE_7,null,cv);
        if(result == -1)
            return  false;
        else
            return true;

    }
    public  long insertOrderCustomer(int orderid,String mobile,String cname,String email,String address){
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("ORDERSID",orderid);
        cv.put("MOBILE",mobile);

        cv.put("NAME",cname);

        cv.put("EMAIL",email);
        cv.put("ADDRESS",address);
        long result = db.insert(TABLE_8,null,cv);

            return result;

    }

    public boolean updateOrderCustomer(Integer cid,Integer orderid, String mobile,String cname,String email,String address) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("ORDERSID",orderid);
        cv.put("MOBILE",mobile);

        cv.put("NAME",cname);

        cv.put("EMAIL",email);
        cv.put("ADDRESS",address);

        long result = db.update(TABLE_8,cv,"ORDERCUSTOMERID ="+ cid,null);
        if(result == -1)
            return  false;
        else
            return true;

    }


    public boolean updateCustomer(Integer cid,String mobile,String cname,String email,String address) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();

        cv.put("MOBILE",mobile);

        cv.put("NAME",cname);

        cv.put("EMAIL",email);
        cv.put("ADDRESS",address);

        long result = db.update(TABLE_7,cv,"CUSTOMERID ="+ cid,null);
        if(result == -1)
            return  false;
        else
            return true;

    }

    public  boolean insertCategories(String cname,String bname){
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("CATEGORIESNAME",cname);

        cv.put("BRANDNAME",bname);

        long result = db.insert(TABLE_1,null,cv);
        if(result == -1)
            return  false;
        else
            return true;

    }
    public  int insertStock(int pid,String date, String qty){
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("PRODUCTID",pid);
        cv.put("SDATE",date);


        cv.put("QUANTITY",qty);

        int result = (int) db.insert(TABLE_5,null,cv);

            return result;

    }

    public void insertOrderitem(Integer orderid, String pname , String pqty,String rate, String ptot){
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("ORDERSID",orderid);


        cv.put("PRODUCTNAME",pname);
        cv.put("QUANTITY",pqty);
        cv.put("RATE",rate);
        cv.put("TOTAL",ptot);

        long result = db.insert(TABLE_3,null,cv);
        if(result == -1) {
        }

    }

    public  long insertOrder(String bdate,String gtotal){
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();


        cv.put("GRANDTOTAL",gtotal);
        cv.put("ORDERDATE",bdate);

        long result = db.insert(TABLE_2,null,cv);

            return result;

    }

    public int insertProductData(String productname, int cattid, String productdetails, String buyprice, String sellprice, String barcode, String productimage, String date) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("PRODUCTNAME",productname);
        cv.put("CATEGORIESID",cattid);
        cv.put("PRODUCTDETAILS",productdetails);
        cv.put("BUYPRICE",buyprice);
        cv.put("SELLPRICE",sellprice);

        cv.put("BARCODE",barcode);
        cv.put("PRODUCTIMAGE",productimage);
        cv.put("DATE",date);
        cv.put("STATUS","ACTIVE");

        int result = (int) db.insert(TABLE_4, null, cv);
            return  result;

    }

    public Cursor getBrandName() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from "+TABLE_1 + " group BY BRANDNAME  ",null);

        return res;

    }
    public Cursor getBrandName(String selectcat) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select BRANDNAME from "+ TABLE_1 + " Where CATEGORIESNAME = '"+ selectcat+"' " +  " group BY BRANDNAME  " ,null);

        return res;

    }

    public Cursor getCategories() {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from "+TABLE_1 + " GROUP BY CATEGORIESNAME ",null);
        return res;
    }

    public boolean updateProductsStock(Integer pid,int qty) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();

        cv.put("STOCKID",qty);

        long result = db.update(TABLE_4,cv,"PRODUCTID ="+pid,null);
        if(result == -1)
            return  false;
        else
            return true;

    }


    public boolean updateProducts(Integer pid,String productname, int cattid, String productdetails,String buyprice,String sellprice,int qty, String barcode,String productimage,String date) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();

        cv.put("PRODUCTNAME",productname);
        cv.put("CATEGORIESID",cattid);
        cv.put("PRODUCTDETAILS",productdetails);
        cv.put("BUYPRICE",buyprice);
        cv.put("SELLPRICE",sellprice);
        cv.put("STOCKID",qty);
        cv.put("BARCODE",barcode);
        cv.put("PRODUCTIMAGE",productimage);
        cv.put("DATE",date);
        cv.put("STATUS","ACTIVE");




        long result = db.update(TABLE_4,cv,"PRODUCTID ="+pid,null);
        if(result == -1)
            return  false;
        else
            return true;

    }

    public Integer getCid(String catt) {
        String query = "SELECT CATEGORIESID FROM " + TABLE_1 + " WHERE " + "CATEGORIESNAME" + " = '" + catt + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery(query, null);
        int id=-1;
        if(res!=null&&res.moveToFirst()) {


            id = res.getInt(res.getColumnIndex("CATEGORIESID"));
        }
        return id;

    }

    public Integer getCid(String catt,String bna) {
        String query = "SELECT CATEGORIESID FROM " + TABLE_1 + " WHERE " + "CATEGORIESNAME" + " = '" + catt + "' AND BRANDNAME = '"+bna+"'" ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery(query, null);
        int id=-1;
        if(res!=null&&res.moveToFirst()) {


            id = res.getInt(res.getColumnIndex("CATEGORIESID"));
        }
        return id;

    }

    public Cursor getProductName() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from "+TABLE_4 + " ORDER BY PRODUCTNAME ASC  ",null);

        return res;


    }

    public List<Products> getProductsbysearch() {

        List<Products> lstproducts = new ArrayList<Products>();


        Cursor cursor = getAllProductsData11();

// Looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Products products = new Products(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7),
                        cursor.getString(8),
                        cursor.getString(9),
                        cursor.getString(10),
                        cursor.getString(11)






                );

                lstproducts.add(products);
            } while (cursor.moveToNext());

        }
        // cursor.close();
        return lstproducts;

    }

    public Cursor getAllProductsData() {

        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery("select * from "+TABLE_4 ,  null);
    }

    public List<Products> getProductsbybrand( String selectbrand) {

        List<Products> lstproductsfiltter = new ArrayList<Products>();


        Cursor cursor = getAllProductsbybrand(selectbrand);

// Looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Products products1 = new Products(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7),
                        cursor.getString(8),
                        cursor.getString(9),
                        cursor.getString(10),
                        cursor.getString(11)





                );

                lstproductsfiltter.add(products1);
            } while (cursor.moveToNext());

        }
        // cursor.close();
        return lstproductsfiltter;

    }

    private Cursor getAllProductsData(String selectcat, String selectbrand) {

                SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery(" SELECT "+ TABLE_4 +".PRODUCTID,"+ TABLE_1 +".CATEGORIESNAME,"+ TABLE_1+".BRANDNAME,"+TABLE_4 +".PRODUCTNAME,"+TABLE_4+".PRODUCTDETAILS,"+ TABLE_4+".BUYPRICE,"+TABLE_4+ ".SELLPRICE,"+TABLE_5+".QUANTITY,"+TABLE_4+".BARCODE,"+TABLE_4+".PRODUCTIMAGE,"+TABLE_4+
                ",.DATE,"+ TABLE_4 + ".STATUS FROM " +TABLE_4 +" INNER JOIN "+ TABLE_1 + " ON " + TABLE_4+ ".CATEGORIESID = " + TABLE_1 + ".CATEGORIESID WHERE CATEGORIESNAME" + " = '" + selectcat + "' AND BRANDNAME = '"+selectbrand+"'" ,null);

        return res;
    }

    private Cursor getAllProductsData11() {

        SQLiteDatabase db = this.getReadableDatabase();



        Cursor res = db.rawQuery(" SELECT "+ TABLE_4 +".PRODUCTID,"+ TABLE_1 +".CATEGORIESNAME,"+ TABLE_1+".BRANDNAME,"+TABLE_4 +".PRODUCTNAME,"+TABLE_4+".PRODUCTDETAILS,"+ TABLE_4+".BUYPRICE,"+TABLE_4+ ".SELLPRICE,"+" (select SUM (QUANTITY) from " + TABLE_5+" WHERE "+ TABLE_4 +".PRODUCTID = "+ TABLE_5+ ".PRODUCTID ),"+TABLE_4+".BARCODE,"+TABLE_4+".PRODUCTIMAGE,"+TABLE_4+
                ".DATE,"+TABLE_4+ ".STATUS FROM " +TABLE_4 +" INNER JOIN "+ TABLE_1 + " ON " + TABLE_4+ ".CATEGORIESID = " + TABLE_1 + ".CATEGORIESID INNER JOIN " + TABLE_5 +" ON " + TABLE_4 +".STOCKID = " +TABLE_5 + ".STOCKID" ,null);

        return res;
    }

    public List<Products> getProductsbysearch(String selectcat) {

        List<Products> lstproductsfiltter = new ArrayList<Products>();


        Cursor cursor = getAllProductsData(selectcat);

// Looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Products products1 = new Products(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7),
                        cursor.getString(8),
                        cursor.getString(9),
                        cursor.getString(10),
                        cursor.getString(11)



                );

                lstproductsfiltter.add(products1);
            } while (cursor.moveToNext());

        }
        // cursor.close();
        return lstproductsfiltter;

    }

    private Cursor getAllProductsData(String selectcat) {
        SQLiteDatabase db = this.getReadableDatabase();

        return db.rawQuery(" SELECT "+ TABLE_4 +".PRODUCTID,"+ TABLE_1 +".CATEGORIESNAME,"+ TABLE_1+".BRANDNAME,"+TABLE_4 +".PRODUCTNAME,"+TABLE_4+".PRODUCTDETAILS,"+ TABLE_4+".BUYPRICE,"+TABLE_4+ ".SELLPRICE,"+"(select SUM (QUANTITY) from " + TABLE_5+" WHERE "+ TABLE_4 +".PRODUCTID = "+ TABLE_5+ ".PRODUCTID ),"+TABLE_4+".BARCODE,"+TABLE_4+".PRODUCTIMAGE,"+TABLE_4+
               ".DATE,"+ TABLE_4 + ".STATUS FROM " +TABLE_4 +" INNER JOIN "+ TABLE_1 + " ON " + TABLE_4+ ".CATEGORIESID = " + TABLE_1 + ".CATEGORIESID INNER JOIN "+ TABLE_5 + " ON " + TABLE_5+ ".STOCKID = " + TABLE_4 + ".STOCKID WHERE CATEGORIESNAME" + " = '" + selectcat + "' " ,null);
    }
    private Cursor getAllProductsbybrand(String selectbr) {
        SQLiteDatabase db = this.getReadableDatabase();

        return db.rawQuery(" SELECT "+ TABLE_4 +".PRODUCTID,"+ TABLE_1 +".CATEGORIESNAME,"+ TABLE_1+".BRANDNAME,"+TABLE_4 +".PRODUCTNAME,"+TABLE_4+".PRODUCTDETAILS,"+ TABLE_4+".BUYPRICE,"+TABLE_4+ ".SELLPRICE,"+"(select SUM (QUANTITY) from " + TABLE_5+" WHERE "+ TABLE_4 +".PRODUCTID = "+ TABLE_5+ ".PRODUCTID ),"+TABLE_4+".BARCODE,"+TABLE_4+".PRODUCTIMAGE,"+TABLE_4+
               ".DATE," +TABLE_4 + ".STATUS FROM " +TABLE_4 +" INNER JOIN "+ TABLE_1 + " ON " + TABLE_4+ ".CATEGORIESID = " + TABLE_1 + ".CATEGORIESID INNER JOIN "+ TABLE_5 + " ON " + TABLE_5+ ".STOCKID = " + TABLE_4 + ".STOCKID WHERE BRANDNAME" + " = '" + selectbr + "' " ,null);
    }

    public List<Products> getProductsbysearchBrand(String selectcat,String selectbr) {

        List<Products> lstproductsfiltter1 = new ArrayList<Products>();


        Cursor cursor = getAllProductsData1(selectcat,selectbr);

// Looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Products products1 = new Products(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7),
                        cursor.getString(8),
                        cursor.getString(9),
                        cursor.getString(10),
                        cursor.getString(11)


                );

                lstproductsfiltter1.add(products1);
            } while (cursor.moveToNext());

        }
        // cursor.close();
        return lstproductsfiltter1;

    }

    public List<Bill> getAllbill() {

        List<Bill> lstBill = new ArrayList<Bill>();


        Cursor cursor = getAllBillData();

// Looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Bill Bill1 = new Bill(

                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2)




                );

                lstBill.add(Bill1);
            } while (cursor.moveToNext());

        }
        // cursor.close();
        return lstBill;

    }

    private Cursor getAllBillData() {
        SQLiteDatabase db = this.getReadableDatabase();



        return db.rawQuery("select ORDERSID,GRANDTOTAL, ORDERDATE FROM "+ TABLE_2 ,  null);
}


    private Cursor getAllProductsData1(String selectcat, String selectbr) {
        SQLiteDatabase db = this.getReadableDatabase();

        return db.rawQuery(" SELECT "+ TABLE_4 +".PRODUCTID,"+ TABLE_1 +".CATEGORIESNAME,"+ TABLE_1+".BRANDNAME,"+TABLE_4 +".PRODUCTNAME,"+TABLE_4+".PRODUCTDETAILS,"+ TABLE_4+".BUYPRICE,"+TABLE_4+ ".SELLPRICE,"+TABLE_4+".QUANTITY,"+TABLE_4+".BARCODE,"+TABLE_4+".PRODUCTIMAGE,"+TABLE_4+
               ".DATE," + TABLE_4+ ".STATUS FROM " +TABLE_4 +" left JOIN "+ TABLE_1 + " ON " + TABLE_4+ ".CATEGORIESID = " + TABLE_1 + ".CATEGORIESID  WHERE   CATEGORIESNAME " + " = ( select CATEGORIESNAME from " + TABLE_1 + " Where CATEGORIESNAME =  '" + selectcat + "') and BRANDNAME " + " = '" + selectbr + "' " ,null);
    }

    public List<Products> getProductsbarcode(String barcode) {

        List<Products> lstproductsfiltter1 = new ArrayList<Products>();

        Cursor cursor = getProductsbybarcode(barcode);

// Looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Products products1 = new Products(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7),
                        cursor.getString(8),
                        cursor.getString(9),
                        cursor.getString(10),
                        cursor.getString(11)

                );

                lstproductsfiltter1.add(products1);
            } while (cursor.moveToNext());

        }
        // cursor.close();
        return  lstproductsfiltter1;

    }

    public Cursor getProductsbybarcode(String barcode) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery(" SELECT "+ TABLE_4 +".PRODUCTID,"+ TABLE_1 +".CATEGORIESNAME,"+ TABLE_1+".BRANDNAME,"+TABLE_4 +".PRODUCTNAME,"+TABLE_4+".PRODUCTDETAILS,"+ TABLE_4+".BUYPRICE,"+TABLE_4+ ".SELLPRICE,"+"(select SUM (QUANTITY) from " + TABLE_5+" WHERE "+ TABLE_4 +".PRODUCTID = "+ TABLE_5+ ".PRODUCTID ),"+TABLE_4+".BARCODE,"+TABLE_4+".PRODUCTIMAGE,"+TABLE_4+
               ".DATE,"+TABLE_4+ ".STATUS FROM " +TABLE_4 +" INNER JOIN "+ TABLE_1 + " ON " + TABLE_4+ ".CATEGORIESID = " + TABLE_1 + ".CATEGORIESID INNER JOIN " + TABLE_5+ " ON " + TABLE_4 +".STOCKID = " + TABLE_5 + ".STOCKID"+  " WHERE BARCODE " + " = '" + barcode + "' and QUANTITY > 0" ,null);



        return res;
    }

    public Cursor getBarcode() {

        SQLiteDatabase db = this.getReadableDatabase();

        return db.rawQuery("select * from "+TABLE_4 ,null);

    }

    public Integer getProductid(String name) {

        String query = "SELECT PRODUCTID FROM " + TABLE_4 + " WHERE " + "PRODUCTNAME" + " = '" + name + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        @SuppressLint("Recycle") Cursor res = db.rawQuery(query, null);
        int id=-1;
        if(res!=null&&res.moveToFirst()) {

            id = res.getInt(res.getColumnIndex("PRODUCTID"));
        }
        return id;
    }

    public Cursor getQtybyid(Integer pid) {
        SQLiteDatabase db = this.getReadableDatabase();

        return db.rawQuery("select SUM(QUANTITY) from "+TABLE_5 + " where PRODUCTID = '"+pid +"'",null);

    }

    public void updateqty(Integer pid, String valueOf) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();

        cv.put("QUANTITY",valueOf);

        db.update(TABLE_5,cv,"PRODUCTID ="+pid,null);

    }

    public List<Bill> getbillproduct() {

        List<Bill> lstBill = new ArrayList<Bill>();


        Cursor cursor = getAllBillproductData();

// Looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Bill Bill1 = new Bill(

                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3)


                );

                lstBill.add(Bill1);
            } while (cursor.moveToNext());

        }
        // cursor.close();
        return lstBill;
    }

    public List<Bill> getbillproduct(String Startdate,String Enddate) {

        List<Bill> lstBill = new ArrayList<Bill>();


        Cursor cursor = getAllBillproductData(Startdate,Enddate);

// Looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Bill Bill1 = new Bill(

                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3)





                );

                lstBill.add(Bill1);
            } while (cursor.moveToNext());

        }
        // cursor.close();
        return lstBill;
    }

    private Cursor getAllBillproductData() {
        SQLiteDatabase db = this.getReadableDatabase();


       Cursor res =  db.rawQuery(" SELECT "+ TABLE_2 +".ORDERSID,"+ TABLE_2 +".ORDERDATE,"+TABLE_3 +".TOTAL,"+TABLE_3+".PRODUCTNAME  " +
               "FROM " +TABLE_2 +" INNER JOIN "+ TABLE_3 + " ON " + TABLE_3+ ".ORDERSID = " + TABLE_2 + ".ORDERSID  ORDER BY  "+ TABLE_3+".PRODUCTNAME ASC "  ,null);

return  res;

    }
    private Cursor getAllBillproductData(String sdate,String edate) {
        SQLiteDatabase db = this.getReadableDatabase();




        Cursor res =  db.rawQuery(" SELECT "+ TABLE_2  +".ORDERSID,"+ TABLE_2 + ".ORDERDATE,"+TABLE_3 +".TOTAL,"+TABLE_3+".PRODUCTNAME  " +
                "FROM " +TABLE_2 +" INNER JOIN "+ TABLE_3 + " ON " + TABLE_3+ ".ORDERSID = " + TABLE_2 + ".ORDERSID  Where " + TABLE_2  +".ORDERDATE BETWEEN date('"+ sdate + "') and date('"+ edate + "') "+ TABLE_4+".PRODUCTNAME ASC "  ,null);

        return  res;

    }

    private Cursor getBillDatabyorderid(int orderid) {
        SQLiteDatabase db = this.getReadableDatabase();




        Cursor res =  db.rawQuery(" SELECT "+ TABLE_2 +".ORDERSID," +TABLE_2 +".ORDERDATE,"+TABLE_3 +".TOTAL,"+ TABLE_3 + ".QUANTITY," + TABLE_3 + ".RATE," + TABLE_3+".PRODUCTNAME  " +
                "FROM " +TABLE_2 +" INNER JOIN "+ TABLE_3 + " ON " + TABLE_3+ ".ORDERSID = " + TABLE_2 + ".ORDERSID   Where " + TABLE_3 + ".ORDERSID = '" + orderid + "'"  ,null);

        return  res;

    }

    public List<Bill> getbillsearch(String startdate, String enddate) {
        List<Bill> lstBill = new ArrayList<Bill>();


        Cursor cursor = getAllBillSearchData(startdate,enddate);

// Looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Bill Bill1 = new Bill(

                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3)




                );

                lstBill.add(Bill1);
            } while (cursor.moveToNext());

        }
        // cursor.close();
        return lstBill;

    }

    private Cursor getAllBillSearchData(String startdate, String enddate) {
        SQLiteDatabase db = this.getReadableDatabase();


     //   return db.rawQuery("select * FROM "+ TABLE_2 ,  null);

       return db.rawQuery("select * FROM "+ TABLE_2 +" WHERE ORDERDATE BETWEEN date('"+ startdate + "') and date('"+ enddate + "')",null);
    }

    public List<Bill> getBillbyno(int orderid) {
        List<Bill> lstBill = new ArrayList<Bill>();


        Cursor cursor = getBillDatabyorderid(orderid);

// Looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Bill Bill1 = new Bill(

                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5)

                );

                lstBill.add(Bill1);
            } while (cursor.moveToNext());

        }
        // cursor.close();
        return lstBill;
    }


    public void deleteproduct(int position){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_4 + " WHERE PRODUCTID = " + position + ";");
        db.execSQL("UPDATE " + TABLE_4 + " SET PRODUCTID = " +" PRODUCTID "
                + " -1 " + " WHERE " + "PRODUCTID" + " > " + position + ";");
        db.close();
    }

    public void deleteBill(int position){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_2 + " WHERE ORDERSID = " + position + ";");
        db.execSQL("DELETE FROM " + TABLE_3 + " WHERE ORDERSID = " + position + ";");
        db.execSQL("UPDATE " + TABLE_2 + " SET ORDERSID = " +" ORDERSID "
                + " -1 " + " WHERE " + "ORDERSID" + " > " + position + ";");
        db.execSQL("UPDATE " + TABLE_3 + " SET ORDERSID = " +" ORDERSID "
                + " -1 " + " WHERE " + "ORDERSID" + " > " + position + ";");
        db.close();
    }

    public int getProductsbyid(int pid) {

        String query = "SELECT SUM (QUANTITY) FROM " + TABLE_5 + " WHERE " + "PRODUCTID" + " = '" + pid + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        @SuppressLint("Recycle") Cursor res = db.rawQuery(query, null);
        int qty=0;
        if(res!=null&&res.moveToFirst()) {

            qty = res.getInt(0);

        }
        return qty;
    }

    public Long UpdateOrder(int id, String date, String total) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();


        cv.put("GRANDTOTAL",total);
        cv.put("ORDERDATE",date);

        long result = db.update(TABLE_2,cv,"ORDERSID = " + id,null);

        return result;
    }

    public Long UpdateOrder(int id, int ocid) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues cv = new ContentValues();


        cv.put("ORDERCUSTOMERID",ocid);


        long result = db.update(TABLE_2,cv,"ORDERSID = " + id,null);

        return result;
    }

    public void deleteOrderitem(int orderid) {
        SQLiteDatabase db = getWritableDatabase();

        db.execSQL("DELETE FROM " + TABLE_3 + " WHERE ORDERSID = " + orderid + ";");

        db.execSQL("UPDATE " + TABLE_3 + " SET ORDERSID = " +" ORDERSID "
                + " -1 " + " WHERE " + "ORDERSID" + " > " + orderid + ";");
        db.close();
    }

    public List<Products> getLowstkock() {
        List<Products> lstproductsfiltter1 = new ArrayList<Products>();

        Cursor cursor = getProductsbyLowsock();

// Looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Products products1 = new Products(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7),
                        cursor.getString(8),
                        cursor.getString(9),
                        cursor.getString(10),
                        cursor.getString(11)

                );

                lstproductsfiltter1.add(products1);
            } while (cursor.moveToNext());

        }
        // cursor.close();
        return  lstproductsfiltter1;

    }

    private Cursor getProductsbyLowsock() {
        SQLiteDatabase db = this.getReadableDatabase();



        Cursor res = db.rawQuery(" SELECT "+ TABLE_4 +".PRODUCTID,"+ TABLE_1 +".CATEGORIESNAME,"+ TABLE_1+".BRANDNAME,"+TABLE_4 +".PRODUCTNAME,"+TABLE_4+".PRODUCTDETAILS,"+ TABLE_4+".BUYPRICE,"+TABLE_4+ ".SELLPRICE,"+"  ( select SUM (QUANTITY)   from " + TABLE_5+" WHERE "+ TABLE_4 +".PRODUCTID = "+ TABLE_5+ ".PRODUCTID ) ,"+TABLE_4+".BARCODE,"+TABLE_4+".PRODUCTIMAGE,"+TABLE_4+
                ".DATE,"+TABLE_4+ ".STATUS FROM " +TABLE_4 +" INNER JOIN "+ TABLE_1 + " ON " + TABLE_4+ ".CATEGORIESID = " + TABLE_1 + ".CATEGORIESID INNER JOIN " + TABLE_5 +" ON " + TABLE_4 +".STOCKID = " +TABLE_5 + ".STOCKID  " ,null);

        return res;
    }

    public int getsalebyweek() {
        String query = "SELECT SUM (GRANDTOTAL) FROM " + TABLE_2 + " WHERE ORDERDATE BETWEEN date('now','localtime','-6 days') AND date('now','localtime')";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery(query, null);
        int bal = 0;
        if (res != null && res.getCount() >= 0) {
            if (res.moveToFirst()) {

                bal = res.getInt(0);
                //return bal;
            }else {
                bal = -1;
                //return bal;
            }


        }

        return bal;
    }

    public int getsalebypreviousweek() {
        String query = "SELECT SUM (GRANDTOTAL) FROM " + TABLE_2 + " WHERE ORDERDATE BETWEEN date('now','localtime','-13 days') AND date('now','localtime','-6 days')";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery(query, null);
        int bal = 0;
        if (res != null && res.getCount() >= 0) {
            if (res.moveToFirst()) {

                bal = res.getInt(0);
                //return bal;
            }else {
                bal = -1;
                //return bal;
            }


        }

        return bal;
    }

    public int getsalebymonth() {
        String query = "SELECT SUM (GRANDTOTAL) FROM " + TABLE_2 + " WHERE ORDERDATE BETWEEN date('now','localtime','start of month') AND date('now','localtime')";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery(query, null);
        int bal = 0;
        if (res != null && res.getCount() >= 0) {
            if (res.moveToFirst()) {

                bal = res.getInt(0);
                //return bal;
            }else {
                bal = -1;
                //return bal;
            }


        }

        return bal;
    }

    public int getsalebypreviousmonth() {
        String query = "SELECT SUM (GRANDTOTAL) FROM " + TABLE_2 + " WHERE ORDERDATE  >= date('now','start of month','-1 months')" +
                "AND ORDERDATE < date('now','start of month')";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery(query, null);
        int bal = 0;
        if (res != null && res.getCount() >= 0) {
            if (res.moveToFirst()) {

                bal = res.getInt(0);
                //return bal;
            }else {
                bal = -1;
                //return bal;
            }


        }

        return bal;
    }

    public int getsalebypreviousyear() {
        String query = "SELECT SUM (GRANDTOTAL) FROM " + TABLE_2 + " WHERE ORDERDATE  >= date('now','start of year','-1 year')" +
                "AND ORDERDATE < date('now','start of year')";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery(query, null);
        int bal = 0;
        if (res != null && res.getCount() >= 0) {
            if (res.moveToFirst()) {

                bal = res.getInt(0);
                //return bal;
            }else {
                bal = -1;
                //return bal;
            }


        }

        return bal;
    }



    public int getsalebyyear() {
        String query = "SELECT SUM (GRANDTOTAL) FROM " + TABLE_2 + " WHERE ORDERDATE BETWEEN date('now','localtime','start of year') AND date('now','localtime')";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery(query, null);
        int bal = 0;
        if (res != null && res.getCount() >= 0) {
            if (res.moveToFirst()) {

                bal = res.getInt(0);
                //return bal;
            } else {
                bal = -1;
                //return bal;
            }


        }
        return bal;
    }

    public void deleteContact(int position){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_7 + " WHERE CUSTOMERID = " + position + ";");

        //  db.execSQL("UPDATE " + TABLE_5 + " SET CONTACTID = " +" CONTACTID "
        //        + " -1 " + " WHERE " + "CONTACTID" + " > " + position + ";");

        db.close();
    }

    public List<Customer> getallCustomer() {

        List<Customer> lstContact = new ArrayList<Customer>();


        Cursor cursor = getAllCustomer();

// Looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Customer Contact = new Customer(
                        cursor.getInt(0),
                        cursor.getString(1),

                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4)



                );

                lstContact.add(Contact);
            } while (cursor.moveToNext());

        }
        //  cursor.close();
        return lstContact;
    }

    private Cursor getAllCustomer() {

        SQLiteDatabase db = this.getReadableDatabase();

        return db.rawQuery("select * from "+TABLE_7 + " ORDER BY NAME ASC  ",null);
    }

    public Cursor getMobileno() {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from "+TABLE_7 ,null);

        return res;
    }

    public List<Customer> getcusomerbymob(String mob) {

        List<Customer> lstContact = new ArrayList<Customer>();


        Cursor cursor = getCustomerbymobile(mob);

// Looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Customer Contact = new Customer(
                        cursor.getInt(0),
                        cursor.getString(1),

                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4)



                );

                lstContact.add(Contact);
            } while (cursor.moveToNext());

        }
        //  cursor.close();
        return lstContact;
    }


    private Cursor getCustomerbymobile(String mob) {

        SQLiteDatabase db = this.getReadableDatabase();

        return db.rawQuery("select * from "+TABLE_7 + " where MOBILE ='" + mob +"'" ,null);
    }
}