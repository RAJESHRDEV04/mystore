package com.example.mystore;

class Datepick {

    public static String parsePickerDate(int Year, int Month, int day) {

        String sYear = String.valueOf(Year);
        String sMonth = String.valueOf((Month));
        String sDay = String.valueOf(day);

        if (sDay.length() == 1)
            sDay = "0" + sDay;

        if (sMonth.length() == 1)
            sMonth = "0" + sMonth;

        if (sDay.length() == 1)
            sDay = "0" + sDay;

        return sYear + "-" + sMonth + "-" + sDay;

    }

}
