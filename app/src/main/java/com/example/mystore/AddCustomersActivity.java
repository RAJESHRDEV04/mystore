package com.example.mystore;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mystore.adapter.CustomerAdapter;
import com.example.mystore.db.DbHelper;
import com.example.mystore.model.Customer;

import java.util.ArrayList;
import java.util.List;

public class AddCustomersActivity extends AppCompatActivity {

    private Button savebtn,clrbtn;
    private EditText nameed,mobileed,addressed,emailed;
    DbHelper myDb;
    private int conid;
    private List<Customer> lstContact;

    CustomerAdapter customerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_customers);

        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        savebtn = findViewById(R.id.btnsave);
        clrbtn = findViewById(R.id.btnclr);
        nameed = findViewById(R.id.edconname);
        mobileed = findViewById(R.id.edmobile);
        addressed = findViewById(R.id.edaddress);
        emailed = findViewById(R.id.edemail);

        myDb = new DbHelper(this);
        lstContact = new ArrayList<Customer>();
        lstContact = myDb.getallCustomer();

        customerAdapter = new CustomerAdapter(this,lstContact);

        getIncomingIntent();

        savebtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {



                        if (savebtn.getText() == "UPDATE") {




                            String name = nameed.getText().toString().trim();
                            String mobile = mobileed.getText().toString().trim();
                            String address = addressed.getText().toString().trim();
                            String email = emailed.getText().toString().trim();

                            if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(mobile) && !TextUtils.isEmpty(address))
                            {

                                boolean isInserted = myDb.updateCustomer( conid, mobile,name, address,email);
                                if (isInserted == true){
                                    Toast.makeText(AddCustomersActivity.this, "CUSTOMER UPDATED", Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(AddCustomersActivity.this, CustomerActivity.class);
                                    startActivity(intent); }
                                else {
                                    Toast.makeText(AddCustomersActivity.this, "CUSTOMER NOT UPDATED", Toast.LENGTH_LONG).show();
                                }
                            }else {
                                Toast.makeText(AddCustomersActivity.this, "Enter all Data", Toast.LENGTH_LONG).show();
                            }
                        } else {

                            String name = nameed.getText().toString().trim();
                            String mobile = mobileed.getText().toString().trim();
                            String address = addressed.getText().toString().trim();
                            String email = emailed.getText().toString().trim();

                            if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(mobile) && !TextUtils.isEmpty(address))
                            {

                                boolean isInserted = myDb.insertCustomer( mobile,name, address,email);
                                if (isInserted == true)
                                    Toast.makeText(AddCustomersActivity.this, "CUSTOMER ADDED", Toast.LENGTH_LONG).show();
                                else
                                    Toast.makeText(AddCustomersActivity.this, "CUSTOMER NOT INSERTED", Toast.LENGTH_LONG).show();
                            }else {
                                Toast.makeText(AddCustomersActivity.this, "Enter all Data", Toast.LENGTH_LONG).show();
                            }

                        }
                    }

                }
        );



        clrbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (clrbtn.getText() == "DELETE") {

                    alert.setTitle("Delete entry");
                    alert.setMessage("Are you sure you want to delete? All data will be deleted with this contact");
                    alert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            customerAdapter.itemRemoved(conid);
                            customerAdapter.notifyDataSetChanged();
                            Intent intent = new Intent(AddCustomersActivity.this, CustomerActivity.class);
                            startActivity(intent);
                        }
                    });
                    alert.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // close dialog
                            dialog.cancel();
                        }
                    });
                    alert.show();





                }else {

                    Clr();
                }

            }
        });
    }

    public void getIncomingIntent(){

        if(getIntent().hasExtra("conid") && getIntent().hasExtra("name")  ){


            int Conid = getIntent().getIntExtra("conid",0);
            String Name = getIntent().getStringExtra("name");
            String mobile = getIntent().getStringExtra("mobile");
            String address = getIntent().getStringExtra("address");
            String email = getIntent().getStringExtra("email");



            setData(Conid, Name,mobile,address,email);
        }
    }

    private void setData(int cid, String name, String mobile,String address,String email) {

        nameed.setText(name);

        mobileed.setText(mobile);
        addressed.setText(address);
        emailed.setText(email);

        conid = cid;

        savebtn.setText("UPDATE");
        clrbtn.setText("DELETE");

    }

    public  void Clr(){
        nameed.setText("");
        mobileed.getText().clear();
        addressed.getText().clear();
        emailed.getText().clear();

    }

}