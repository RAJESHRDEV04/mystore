package com.example.mystore;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mystore.adapter.BillRecAdapter;
import com.example.mystore.adapter.BillRecEditAdapter;
import com.example.mystore.adapter.ProductsAdapter;
import com.example.mystore.adapter.ViewPagerAdapter;
import com.example.mystore.db.DbHelper;
import com.example.mystore.fragment.FragmentBillTotal;
import com.example.mystore.fragment.FragmentSearchProducts;
import com.example.mystore.fragment.FragmentTotal;
import com.example.mystore.model.Bill;
import com.example.mystore.ui.main.BillViewFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.example.mystore.fragment.FragmentTotal.tvtotal;

public class BillActivity extends AppCompatActivity {

    int Ordersid;
    public static List<Bill> listbill;
    Button btnprint,btndelete;
    ImageView btnhome,btnedit;
    TextView tvbilldate,tvbillno,tvtotalqty,tvtotalamnt,tvbill;
    public static RecyclerView recyclerviewbill;
    DbHelper myDb;
    BillRecAdapter billRecAdapter;
    BillRecEditAdapter billRecEditAdapter;
     ViewPager viewPager ;
     ViewPagerAdapter viewPagerAdapter1 ;
    int year,month,day;
    String billdate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill);




        final AlertDialog.Builder alert = new AlertDialog.Builder(this);

        btnprint = findViewById(R.id.btnprint);
        btndelete = findViewById(R.id.btndelete);
        tvbilldate = findViewById(R.id.tvbilldate);
        tvbillno = findViewById(R.id.tvbillno);
        tvbill = findViewById(R.id.tvbill);
        tvtotalqty  = findViewById(R.id.tvtotalqty);
        tvtotalamnt = findViewById(R.id.tvtotalamnt);
        recyclerviewbill = findViewById(R.id.recyclerViewbill);
        btnhome=findViewById(R.id.btnhome);
        btnedit = findViewById(R.id.btnedit);

        myDb = new DbHelper(this);
        listbill = new ArrayList<Bill>();


        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);


       viewPager = findViewById(R.id.viewtotalamnt);

       tvbilldate.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

              if(btnprint.getText().toString().equals("Update")) {


                  DatePickerDialog datePickerDialog = new DatePickerDialog(
                          BillActivity.this,R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                      @Override
                      public void onDateSet(DatePicker view, int year, int month, int day) {
                          month = month + 1;
                          //String date = day+"/"+month+"/"+year;

                          String date = Datepick.parsePickerDate(year, month, day);
                          tvbilldate.setText(Dateformatchange.Dchange(date));

                      }
                  }, year, month, day);
                  datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                  datePickerDialog.show();
              }
           }
       });

        btnedit.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {


                recyclerviewbill.setHasFixedSize(true);
                billRecEditAdapter = new BillRecEditAdapter(BillActivity.this,listbill);
                recyclerviewbill.setLayoutManager(new LinearLayoutManager(BillActivity.this));
                recyclerviewbill.setAdapter(billRecEditAdapter);

                btnprint.setText("Update");
            }
        });

        btnprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(btnprint.getText().toString().equals("Print")){

                }else {
                    if(!listbill.isEmpty())
                    {

                        recyclerviewbill.setItemViewCacheSize(listbill.size());

                        for (int i = 0; i < listbill.size(); i++) {

                        }

                        String date = Dateformatchange.Ychange( tvbilldate.getText().toString());
                        String total = BillViewFragment.tvtotal.getText().toString();
                        Long orderid = null;
                        if(!date.equals(billdate)) {
                             orderid = myDb.UpdateOrder(Integer.parseInt(tvbillno.getText().toString()), date, total);
                        }
                        if (orderid == -1){
                            Toast.makeText(BillActivity.this, "Somthing wen't worng, bill not generated", Toast.LENGTH_LONG).show();

                        }else {
                            myDb.deleteOrderitem(Ordersid);


                            for (int i = 0; i < listbill.size(); i++) {

                                View view = recyclerviewbill.getChildAt(i);
                                TextView pname = (TextView) view.findViewById(R.id.tvproductname);
                                EditText pq = (EditText) view.findViewById(R.id.tvquantity);
                                TextView pt = (TextView) view.findViewById(R.id.tvamnt);
                                TextView ratetv = (TextView) view.findViewById(R.id.tvrate);

                                int qtyy = 0;
                                String qty = pq.getText().toString();
                                String ptotal = pt.getText().toString();
                                String name = pname.getText().toString();
                                String rate = ratetv.getText().toString();
                                Integer Pid = myDb.getProductid(name);

                                Cursor cursor = myDb.getQtybyid(Pid);
                                if (cursor != null){
                                    while(cursor.moveToNext()){
                                        qtyy = Integer.parseInt((cursor.getString(cursor.getColumnIndex("QUANTITY"))));
                                    }
                                    cursor.close();
                                }
                               int  lstqty = Integer.parseInt(listbill.get(i).getQty());
                                qtyy = qtyy + lstqty - Integer.parseInt(qty);

                                myDb.updateqty(Pid,String.valueOf(qtyy));
                                myDb.insertOrderitem(Integer.parseInt(String.valueOf(orderid)),name,qty,rate,ptotal);

                                Intent intent = new Intent(BillActivity.this,ReportActivity.class);
                                startActivity(intent);
                            }
                        }

                        // tvtotal.setText("00");

                        //   pr.printInvoice();
                    }else {
                        Toast.makeText(BillActivity.this, "Cart is Empty", Toast.LENGTH_LONG).show();

                    }
                }
            }
        });
        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.setTitle("Delete entry");
                alert.setMessage("Are you sure you want to delete? All data will be deleted with this Bill");
                alert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete


                        for(Bill item : listbill){
                            int qty = Integer.parseInt(item.getQty());
                            String name = item.getProductname();
                    


                            Integer Pid = myDb.getProductid(name);
                            int qtyy = 0;
                            Cursor cursor = myDb.getQtybyid(Pid);
                            if (cursor != null){
                                while(cursor.moveToNext()){
                                    qtyy = Integer.parseInt((cursor.getString(cursor.getColumnIndex("QUANTITY"))));
                                }
                                cursor.close();
                            }
                            myDb.updateqty(Pid, String.valueOf(qtyy + qty));


                        }
                        myDb.deleteBill(Ordersid);
                        Intent intent = new Intent(BillActivity.this, ReportActivity.class);
                        startActivity(intent);
                    }
                });
                alert.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // close dialog
                        dialog.cancel();
                    }
                });
                alert.show();

            }
        });
        btnhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BillActivity.this,MainActivity.class);
                startActivity(intent);

            }
        });



        getIntentData();

        recyclerviewbill.setHasFixedSize(true);
        billRecAdapter = new BillRecAdapter(this,listbill);
        recyclerviewbill.setLayoutManager(new LinearLayoutManager(this));
        recyclerviewbill.setAdapter(billRecAdapter);


            new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                    ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                @Override
                public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                     String edit = btnprint.getText().toString();
                    if (edit.equals("Update")) {

                        billRecAdapter.deleteItem(viewHolder.getAdapterPosition());


                    }else {
                        recyclerviewbill.setAdapter(billRecAdapter);
                    }

                }
            }).attachToRecyclerView(recyclerviewbill);

        final Handler handler = new Handler();
        Timer timer = new Timer();
        TimerTask doTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @SuppressWarnings("unchecked")
                    public void run() {
                        try {
                            viewPagerAdapter1 = new ViewPagerAdapter(getSupportFragmentManager());
                            viewPagerAdapter1.AddFragment(new BillViewFragment(),"lstProducts");
                            viewPager.setAdapter(viewPagerAdapter1);
                        }
                        catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });
            }
        };
        timer.schedule(doTask, 0, 10);




        //  viewPagerAdapter1.AddFragment(new FragmentBillTotal(),"lstProducts");

     //   viewPagerAdapter1.replaceFragment(0, new FragmentBillTotal(),"lstProducts");
       // viewPagerAdapter1.notifyDataSetChanged();

      //  viewPagerAdapter1.AddFragment(new FragmentBillTotal(),"lstProducts");
        //    getSupportFragmentManager().beginTransaction().add(R.id.container, newInstance(selectcat,selectbrand),"lstProducts").commit();

        //viewPager.setAdapter(viewPagerAdapter1);

    }

    @SuppressLint("SetTextI18n")
    private void getIntentData() {

        if(getIntent().hasExtra("ordersid")){
            Ordersid = getIntent().getIntExtra("ordersid",0);

            listbill.clear();
            listbill = myDb.getBillbyno(Ordersid);


            int qty = 0;
            int tot = 0;
           for(Bill item : listbill){
               qty = qty + Integer.parseInt(item.getQty());
               tot = tot + Integer.parseInt(item.getTotal());
           }
            billdate = Dateformatchange.Dchange( listbill.get(0).getDate());
            tvbilldate.setText(billdate);
            tvbillno.setText(String.valueOf(Ordersid));



        }

    }


}