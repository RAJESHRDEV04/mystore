package com.example.mystore.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;


import com.example.mystore.Dateformatchange;
import com.example.mystore.Imageconverter;


import com.example.mystore.ProductsRecycleClick;
import com.example.mystore.R;

import com.example.mystore.fragment.FragmentBillReport;
import com.example.mystore.fragment.FragmentProductReport;
import com.example.mystore.model.Bill;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProductReportAdapter extends RecyclerView.Adapter <ProductReportAdapter.BillViewHolder>{

    private Context mContext;
    List<Bill> mData;
    Dialog  myDialog;
    //   private BillRecycleClick itemClickListener;

    private static ProductsRecycleClick itemClickListener;

    public ProductReportAdapter(Context context, List<Bill> cursor) {
        mContext = context;
        mData = cursor;
    }

    public ProductReportAdapter() {
    }


    @NonNull
    @Override
    public BillViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.bill_report_rc, parent, false);
        final BillViewHolder viewHolder = new BillViewHolder(view);

        //   myDialog = new Dialog(mContext);
        // myDialog.setContentView(R.layout.productfulldescription);

        return new BillViewHolder(view);

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull BillViewHolder holder, final int position) {

        final String productname = mData.get(position).getProductname();


        final String amt = mData.get(position).getTotal();
        final String date1 =  mData.get(position).getDate();

        holder.amttv.setText(amt);
        holder.Billnotv.setText(String.valueOf( productname));
        holder.datetv.setText(Dateformatchange.Dchange(date1));


    }

    public int getvalue(int i){


        return Integer.parseInt( mData.get(i).getTotal());

    }
    public  int gettotalamt(){
        int tot = 0;

        if(mData != null){
            for(int i =0 ;i< mData.size();i++ ){
                tot = tot + Integer.parseInt(mData.get(i).getTotal());
            }
            return tot;
        }else {
            return 0;
        }
    }
    public int getsize(){
        if(mData != null){
            return mData.size();
        }else {
            return 0;
        }
    }

    public void filterList(ArrayList<Bill> filteredList) {
        mData = filteredList;
        notifyDataSetChanged();
    }

    private Drawable showimage(String productimage) {

        Imageconverter  imageconverter = new Imageconverter();

        Bitmap image = imageconverter.stringToBitmap(productimage);
        Drawable d = new BitmapDrawable(mContext.getResources(), image);
        return  d;
    }


    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void  setClickListener(ProductsRecycleClick itemClickListener){


        this.itemClickListener = itemClickListener ;

    }

    public int gettotal() {


        return 0;
    }

    public static class BillViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {



        private ConstraintLayout rc_Bill;

        private TextView Billnotv;
        private TextView amttv;
        private TextView datetv;
        //   private TextView datetv;


        public BillViewHolder(@NonNull View itemView) {
            super(itemView);
            Billnotv = itemView.findViewById(R.id.tvbillno);
            amttv = itemView.findViewById(R.id.tvprice);
            datetv = itemView.findViewById(R.id.tvdate);
            rc_Bill = itemView.findViewById(R.id.rcproductreport);

            //  itemView.setOnClickListener((View.OnClickListener) this);

        }
        @Override
        public void onClick(View v) {
            if(itemClickListener != null){
                itemClickListener.onClick(v, getAdapterPosition());
            }
        }


    }



}
