package com.example.mystore.adapter;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mystore.AddproductsActivity;
import com.example.mystore.Imageconverter;

import com.example.mystore.R;
import com.example.mystore.SaleActivity;
import com.example.mystore.model.Products;

import java.util.ArrayList;
import java.util.List;

import static com.example.mystore.SaleActivity.SalesAllRcView;
import static com.example.mystore.SaleActivity.lstProducts;
import static com.example.mystore.SaleActivity.salesAdaptor;

public class SalesAdapter extends RecyclerView.Adapter <SalesAdapter.SalesViewHolder>{

    private Context mContext;
    List<Products> mData;
    int totamnt;
    int pqty =1;
    int eqty =1;
    Dialog  myDialog;
    public SalesAdapter() {
    }
//   private SalesRecycleClick itemClickListener;

  //  private static SalesRecycleClick itemClickListener;

    public SalesAdapter(Context context, List<Products> cursor) {
        mContext = context;
        mData = cursor;

    }




    @NonNull
    @Override
    public SalesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.salesrecycler, parent, false);
        final SalesViewHolder viewHolder = new SalesViewHolder(view);


        myDialog = new Dialog(mContext);
        myDialog.setContentView(R.layout.addstock);
        return new SalesViewHolder(view);

    }


    @Override
    public void onBindViewHolder(@NonNull final SalesViewHolder holder, final int position) {



        final int  productid = mData.get(position).getProductid();
        final String productname =  mData.get(position).getProductname();

        // final String date = mData.get(position).getDate();

        final String productimage = mData.get(position).getProductimage();
        final int category = mData.get(position).getCategoryid();
        final String barcode = mData.get(position).getBarcode();
        final String buyprice = mData.get(position).getBuyprice();
        final String sellprice = mData.get(position).getSellprice();
        final String catt1 = mData.get(position).getCategory();
        final int quantity = Integer.parseInt(mData.get(position).getQuantity()) ;


        holder.pimage.setImageDrawable(showimage(productimage));
        holder.nametv.setText(productname);
        holder.amttv.setText(sellprice);
        holder.cattv.setText(catt1);
        holder.amttv.setTextColor(Color.RED);


        if (Integer.parseInt( holder.qtytv.getText().toString())==1){
            holder.totamttv.setText(sellprice);

        }
        else {
            holder.totamttv.setText(String.valueOf( Integer.parseInt( holder.qtytv.getText().toString())* Integer.parseInt( sellprice)));

        }
        //holder.qtytv.setText("1");


        if(position == mData.size()-1) {
            totamnt = totamnt + pqty * Integer.parseInt((String) holder.totamttv.getText());
        }
   //     SaleActivity.tvtotamt.setText(String.valueOf( totamnt));
        SaleActivity.listtotal = new ArrayList<>();


        SaleActivity.listtotal.add(holder.totamttv.getText().toString());
    //   SaleActivity.gettotall();
      holder.totamttv.setOnFocusChangeListener(new View.OnFocusChangeListener() {
          @Override
          public void onFocusChange(View v, boolean hasFocus) {


              if (!hasFocus) {
                  String data=holder.totamttv.getText().toString();

                  SaleActivity.listtotal.set(position, data);
             //     SaleActivity.gettotall();

              }

              if(position== mData.size()-1) {
                  holder.totamttv.addTextChangedListener(new TextWatcher() {
                      @Override
                      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                      }

                      @Override
                      public void onTextChanged(CharSequence s, int start, int before, int count) {

                      }

                      @Override
                      public void afterTextChanged(Editable s) {
           //               SaleActivity.listtotal.add(s.toString());
         //                 SaleActivity.gettotall();
                      }
                  });

              }

          }
      });



      holder.qtytv.setOnClickListener(new View.OnClickListener() {
          @SuppressLint("SetTextI18n")
          @Override
          public void onClick(View v) {


              TextView proname = myDialog.findViewById(R.id.tvproname);
              final EditText proqty = myDialog.findViewById(R.id.edquantity);
              Button addqtybtn = myDialog.findViewById(R.id.btnaddbr);

              addqtybtn.setText("Add");
              proname.setText(productname);
              myDialog.show();
              addqtybtn.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {

                      int prqty = Integer.parseInt(proqty.getText().toString());
                      if(prqty !=0 && String.valueOf(proqty.getText().toString().trim()) != null)
                      {
                          int mqty = 0;

                          int j = holder.getAdapterPosition();

                          for (int i = 0; i < mData.size(); i++) {
                              if (mData.get(i).getBarcode().equals(barcode) && holder.getAdapterPosition() != i) {
                                  View view = SalesAllRcView.getChildAt(i);

                                  EditText pq = (EditText) view.findViewById(R.id.edquantity);

                                  mqty = mqty + Integer.parseInt(pq.getText().toString());
                              }
                          }


                          if (prqty + mqty > quantity) {
                              Toast.makeText(mContext, "Out Of Stock", Toast.LENGTH_LONG).show();

                          } else {

                              holder.qtytv.setText(proqty.getText().toString());
                              myDialog.dismiss();
                          }


                      }else  {
                          Toast.makeText(mContext, "Enter Valid Number", Toast.LENGTH_LONG).show();
                      }
                  }
              });
          }
      });

        holder.qtytv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }


            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                    int pqty = Integer.parseInt(String.valueOf(s));


                        holder.totamttv.setText(String.valueOf((pqty * Integer.parseInt(sellprice))));


                    //   if(position == mData.size()-1) {
                    totamnt += Integer.parseInt((String) holder.totamttv.getText());
                    // }
                    //totamnt += Integer.parseInt((String) holder.totamttv.getText());
                    //   SaleActivity.tvtotamt.setText(String.valueOf( totamnt));

            }
        });

        holder.qadd.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {

                int mqty =0;

                for(int i =0; i < mData.size();i++ ){
                    if(mData.get(i).getBarcode().equals(barcode) && holder.getAdapterPosition()!= i){
                        View view = SalesAllRcView.getChildAt(i);

                        EditText pq = (EditText) view.findViewById(R.id.edquantity);

                        mqty =  mqty+ Integer.parseInt(pq.getText().toString());
                    }
                }


                if ( Integer.parseInt(holder.qtytv.getText().toString()) + mqty >= quantity){
                    Toast.makeText(mContext, "Out Of Stock", Toast.LENGTH_LONG).show();
                }
                else {
                    pqty =  Integer.parseInt(holder.qtytv.getText().toString());
                    pqty = pqty+1;
                    if(pqty<10){
                        holder.qtytv.setText("0"+ pqty);
                    }else {
                        holder.qtytv.setText(String.valueOf(pqty));
                    }

                }



                //  holder.totamttv.setText(String.valueOf((pqty* Integer.parseInt(sellprice))));
            }


        });


        holder.qminus.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                int pqty =  Integer.parseInt(holder.qtytv.getText().toString());
                if(pqty >1 )
                {
                    pqty = pqty-1;


                    if(pqty<10){
                        holder.qtytv.setText("0"+ pqty);
                    }else {
                        holder.qtytv.setText(String.valueOf(pqty));
                    }


                   // holder.totamttv.setText(String.valueOf((pqty* Integer.parseInt(sellprice))));

                }
            }
        });
        holder.imgviewdelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 SaleActivity.lstProducts.remove(holder.getAdapterPosition());

                 holder.qtytv.setText("01");
                 salesAdaptor.setData(lstProducts);
                 //notifyItemChanged(holder.getAdapterPosition());
               //notifyDataSetChanged();
            }
        });




    }


    public void setData(List<Products> productsList){
        this.mData =productsList;
        notifyDataSetChanged();
    }

    private Drawable showimage(String productimage) {

        Imageconverter  imageconverter = new Imageconverter();

        Bitmap image = imageconverter.stringToBitmap(productimage);
        Drawable d = new BitmapDrawable(mContext.getResources(), image);
        return  d;
    }


    @Override
    public int getItemCount() {
        return mData.size();
    }




    public static class SalesViewHolder extends RecyclerView.ViewHolder  {



        private ConstraintLayout rc_Sales;
        private ImageView pimage;
        private TextView nametv;
        private TextView amttv;
        private TextView cattv;
        private EditText qtytv;
        private ImageView qadd,imgviewdelete;
        private ImageView qminus;

         TextView totamttv;

        //   private TextView datetv;


        @SuppressLint("SetTextI18n")
        public SalesViewHolder(@NonNull View itemView) {
            super(itemView);
            nametv = itemView.findViewById(R.id.tvbillno);
            amttv = itemView.findViewById(R.id.tvAmnt);
            cattv = itemView.findViewById(R.id.tvcategory);
            qtytv = itemView.findViewById(R.id.edquantity);
            pimage = itemView.findViewById(R.id.imgviewpro);
            rc_Sales = itemView.findViewById(R.id.salerc);
            totamttv = itemView.findViewById(R.id.amnt);
            imgviewdelete = itemView.findViewById(R.id.imgviewdelete);
            qadd = itemView.findViewById(R.id.imgviewqadd);
            qminus = itemView.findViewById(R.id.imgviewqminus);

            qtytv.setText("01");


        }


    }




    public  void datachange(){
        this.notifyDataSetChanged();
    }

}
