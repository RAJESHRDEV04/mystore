package com.example.mystore.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.mystore.Dateformatchange;
import com.example.mystore.Imageconverter;


import com.example.mystore.ProductsRecycleClick;
import com.example.mystore.R;

import com.example.mystore.db.DbHelper;
import com.example.mystore.fragment.FragmentBillReport;
import com.example.mystore.model.Bill;


import java.util.List;

public class BillRecEditAdapter extends RecyclerView.Adapter <BillRecEditAdapter.BillViewHolder>{

    private Context mContext;
    public List<Bill> mData;
    int totamnt;
    int pqty =1;
    DbHelper myDb;
    // Dialog  myDialog;
    //   private BillRecycleClick itemClickListener;

    private static ProductsRecycleClick itemClickListener;

    public BillRecEditAdapter(Context context, List<Bill> cursor) {
        mContext = context;
        mData = cursor;
    }

    public BillRecEditAdapter() {

    }

    @NonNull
    @Override
    public BillViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.billdataedit_rcview, parent, false);
        final BillViewHolder viewHolder = new BillViewHolder(view);

        //  myDialog = new Dialog(mContext);
        //  myDialog.setContentView(R.layout.billdata);

        myDb = new DbHelper(mContext);
        return new BillViewHolder(view);


    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final BillViewHolder holder, final int position) {

          final int  orderid = mData.get(position).getOrderid();

        final String date = mData.get(position).getDate();
        final String amt = mData.get(position).getTotal();
        final String pname = mData.get(position).getProductname();
        final String qty = mData.get(position).getQty();
        final String rate = mData.get(position).getRate();
        holder.pnametv.setText(pname);
        holder.amttv.setText(amt);
      //  holder.qtytv.setText(qty);
        if(Integer.parseInt(qty) <10){
            holder.qtytv.setText(""+ qty);
        }else {
            holder.qtytv.setText(qty);
        }
        holder.ratetv.setText(rate);

        int pid = myDb.getProductid(pname);



        final int prqty = Integer.parseInt(qty) + myDb.getProductsbyid(pid);
        holder.qtytv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                int  pqty =  Integer.parseInt(String.valueOf(s));
                totamnt = totamnt-Integer.parseInt((String) holder.amttv.getText());

            }


            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                int  pqty =  Integer.parseInt(String.valueOf(s));
             //
                holder.amttv.setText(String.valueOf((pqty* Integer.parseInt(rate))));

                //   if(position == mData.size()-1) {
                totamnt  += Integer.parseInt((String) holder.amttv.getText());
                // }
                //totamnt += Integer.parseInt((String) holder.totamttv.getText());
                //   SaleActivity.tvtotamt.setText(String.valueOf( totamnt));
            }
        });

        holder.qadd.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                int pqty =  Integer.parseInt(holder.qtytv.getText().toString());
                if (pqty >= prqty){
                    Toast.makeText(mContext, "Out Of Stock", Toast.LENGTH_LONG).show();
                }
                else {
                    pqty =  Integer.parseInt(holder.qtytv.getText().toString());
                    pqty = pqty+1;
                    if(pqty<10){
                        holder.qtytv.setText("0"+ pqty);
                    }else {
                        holder.qtytv.setText(String.valueOf(pqty));
                    }

                }





                //  holder.totamttv.setText(String.valueOf((pqty* Integer.parseInt(sellprice))));
            }
        });



        holder.qminus.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                int pqty =  Integer.parseInt(holder.qtytv.getText().toString());
                if(pqty >1 )
                {
                    pqty = pqty-1;


                    if(pqty<10){
                        holder.qtytv.setText("0"+ pqty);
                    }else {
                        holder.qtytv.setText(String.valueOf(pqty));
                    }


                    // holder.totamttv.setText(String.valueOf((pqty* Integer.parseInt(sellprice))));

                }
            }
        });

    }



    private Drawable showimage(String productimage) {

        Imageconverter  imageconverter = new Imageconverter();

        Bitmap image = imageconverter.stringToBitmap(productimage);
        Drawable d = new BitmapDrawable(mContext.getResources(), image);
        return  d;
    }


    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void  setClickListener(ProductsRecycleClick itemClickListener){


        this.itemClickListener = itemClickListener ;

    }

    public static class BillViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {



        private ConstraintLayout rc_Bill;


        private TextView amttv;
        private TextView pnametv;
        private EditText qtytv;
        private TextView ratetv;
        private ImageView qadd;
        private ImageView qminus;

        public BillViewHolder(@NonNull View itemView) {
            super(itemView);

            amttv = itemView.findViewById(R.id.tvamnt);
            qtytv = itemView.findViewById(R.id.tvquantity);
            ratetv = itemView.findViewById(R.id.tvrate);
            pnametv = itemView.findViewById(R.id.tvproductname);
            rc_Bill = itemView.findViewById(R.id.recyviewbill);
            qadd = itemView.findViewById(R.id.imgviewqadd);
            qminus = itemView.findViewById(R.id.imgviewqminus);

            //  itemView.setOnClickListener((View.OnClickListener) this);

        }
        @Override
        public void onClick(View v) {
            if(itemClickListener != null){
                itemClickListener.onClick(v, getAdapterPosition());
            }
        }


    }




}



