package com.example.mystore.adapter;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mystore.AddproductsActivity;
import com.example.mystore.BillActivity;
import com.example.mystore.Dateformatchange;
import com.example.mystore.Imageconverter;
import com.example.mystore.ProductsActivity;
import com.example.mystore.ProductsRecycleClick;
import com.example.mystore.R;
import com.example.mystore.ReportActivity;
import com.example.mystore.SaleActivity;
import com.example.mystore.db.DbHelper;
import com.example.mystore.model.Bill;
import com.example.mystore.model.Products;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.example.mystore.AddproductsActivity.parsePickerDate;

public class ProductsAdapter extends RecyclerView.Adapter <ProductsAdapter.ProductsViewHolder>{

    private Context mContext;
     List<Products> mData;
    Dialog  myDialog,myDialogstock;
    int year,month,day;
    String cdate;
    DbHelper myDb;
    //   private ProductsRecycleClick itemClickListener;

    private static ProductsRecycleClick itemClickListener;

    public ProductsAdapter(Context context, List<Products> cursor) {
        mContext = context;
        mData = cursor;
    }

    @NonNull
    @Override
    public ProductsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.products_rcview, parent, false);
        final ProductsViewHolder viewHolder = new ProductsViewHolder(view);

        myDialog = new Dialog(mContext);

        myDialog.setContentView(R.layout.productfulldescription);
        myDialogstock = new Dialog(mContext);
        myDialogstock.setContentView(R.layout.addstock);


        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        cdate =  parsePickerDate(year,month+1,day);


        return new ProductsViewHolder(view);

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ProductsViewHolder holder, final int position) {

        final int  productid = mData.get(position).getProductid();
        final String productname =  mData.get(position).getProductname();

        // final String date = mData.get(position).getDate();

        final String productimage = mData.get(position).getProductimage();
        final int category = mData.get(position).getCategoryid();
        final  String Cate = mData.get(position).getCategory();
        final String barcode = mData.get(position).getBarcode();
        final String buyprice = mData.get(position).getBuyprice();
        final String sellprice = mData.get(position).getSellprice();
        final String qty1 = mData.get(position).getQuantity();
        final String date = mData.get(position).getDate();
        final String pdes = mData.get(position).getProductdetails();
        final String pbrand = mData.get(position).getBrandname();

        holder.pimage.setImageDrawable(showimage(productimage));
        holder.nametv.setText(productname);
        holder.amttv.setText(sellprice);
       // holder.amttv.setTextColor(Color.RED);
        final int iqty = Integer.parseInt(qty1);
        if( iqty == 0){

            holder.qtytv.setText("No Stock");
            holder.qtytv.setTextColor(Color.RED);

        }else{
            if(iqty <10){
                holder.qtytv.setText("0"+iqty);
            }else {
                holder.qtytv.setText(qty1);
            }

        }

        holder.btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final TextView proname = myDialogstock.findViewById(R.id.tvproname);
                final EditText proqty = myDialogstock.findViewById(R.id.edquantity);
                Button addbtn = myDialogstock.findViewById(R.id.btnaddbr);
                cdate = parsePickerDate(year,month,day);
                proname.setText(Dateformatchange.Dchange(cdate));

                myDialogstock.show();

                proname.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DatePickerDialog datePickerDialog = new DatePickerDialog(
                                mContext, new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int day) {
                                month = month+1;
                                //String date = day+"/"+month+"/"+year;

                                cdate = parsePickerDate(year,month,day);
                                proname.setText(Dateformatchange.Dchange(cdate));

                            }
                        },year,month,day);
                        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                        datePickerDialog.show();
                    }
                });

                addbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pqtyy = Integer.parseInt(proqty.getText().toString());
                        String proqtty = proqty.getText().toString();
                        if(pqtyy != 0 && proqtty != null){
                            myDb = new DbHelper(mContext);
                            myDb.insertStock(productid,cdate,proqtty);
                            Toast.makeText(mContext, "Stock Added", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(mContext, ProductsActivity.class);
                            mContext.startActivity(intent);
                            myDialogstock.dismiss();

                        }else {
                            Toast.makeText(mContext, "Enter Vaild Number", Toast.LENGTH_LONG).show();
                        }

                    }
                });

            }
        });

        holder.rc_Products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TextView pname = myDialog.findViewById(R.id.tvbillno);
                TextView pcat = myDialog.findViewById(R.id.tvprocat);
                final TextView brand = myDialog.findViewById(R.id.tvprobrand);
                final TextView qty = myDialog.findViewById(R.id.tvproqty);
                final TextView pdesc = myDialog.findViewById(R.id.tvprodesc);
                TextView pbyprice = myDialog.findViewById(R.id.tvbuyingprice);
                TextView pslprice = myDialog.findViewById(R.id.tvsellingprice);
                TextView pbarcode = myDialog.findViewById(R.id.tvbarcodeno);
                Button deletebtn = myDialog.findViewById(R.id.btndelete2);
                ImageView pimage1 = myDialog.findViewById(R.id.imgviewproduct);
                final ImageView closedbox = myDialog.findViewById(R.id.imgviewclose);
                Button pedit = myDialog.findViewById(R.id.btnedit);


                pimage1.setImageDrawable(showimage(productimage));
                pname.setText(mData.get(position).getProductname());
                pcat.setText(mData.get(position).getCategory());
                pbarcode.setText(mData.get(position).getBarcode());
                qty.setText(mData.get(position).getQuantity());
                pdesc.setText(mData.get(position).getProductdetails());
                pbyprice.setText(mData.get(position).getBuyprice());
                pslprice.setText(mData.get(position).getSellprice());
                brand.setText(mData.get(position).getBrandname());
                myDialog.show();

                deletebtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final AlertDialog.Builder alert = new AlertDialog.Builder(mContext);

                        alert.setTitle("Delete entry");
                        alert.setMessage("Are you sure you want to delete? All data will be deleted with this Product");
                        alert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                itemRemove(position, productid);
                                myDialog.dismiss();
                                Intent intent = new Intent(mContext, ProductsActivity.class);
                                mContext.startActivity(intent);

                            }
                        });
                        alert.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // close dialog
                                dialog.cancel();
                            }
                        });
                        alert.show();



                    }
                });

                pedit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Intent intent = new Intent(mContext, AddproductsActivity.class);
                        intent.putExtra("pid", productid);
                        intent.putExtra("pname", productname);
                        intent.putExtra("cat", Cate);
                        intent.putExtra("brand", pbrand);
                        intent.putExtra("qty", qty1);
                        intent.putExtra("date", date);
                        intent.putExtra("pdesc", pdes);
                        intent.putExtra("buyprice", buyprice);
                        intent.putExtra("sellprice", sellprice);
                        intent.putExtra("barcode", barcode);
                        intent.putExtra("pimage", productimage);
                        myDialog.dismiss();

                        mContext.startActivity(intent);

                    }
                });
                closedbox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });



            }
        });



    }

    public void filterList(ArrayList<Products> filteredList) {
        mData = filteredList;
        notifyDataSetChanged();
    }

    private Drawable showimage(String productimage) {

        Imageconverter  imageconverter = new Imageconverter();

        Bitmap image = imageconverter.stringToBitmap(productimage);
        Drawable d = new BitmapDrawable(mContext.getResources(), image);
        return  d;
    }


    @Override
    public int getItemCount() {
        return mData.size();
    }

     public void  setClickListener(ProductsRecycleClick itemClickListener){
       this.itemClickListener = itemClickListener ;

    }

    public static class ProductsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {



          private final ConstraintLayout rc_Products;
          private final ImageView pimage;
          private final TextView nametv;
          private final TextView amttv;
          private final TextView qtytv;
        //   private TextView datetv;
          private  Button btnadd;


        public ProductsViewHolder(@NonNull View itemView) {
            super(itemView);
              nametv = itemView.findViewById(R.id.tvproductname);
              amttv = itemView.findViewById(R.id.tvprice);
              qtytv = itemView.findViewById(R.id.tvqty);
              pimage = itemView.findViewById(R.id.imgproduct);
              btnadd = itemView.findViewById(R.id.btnadd);
              rc_Products = itemView.findViewById(R.id.rcview);

          //  itemView.setOnClickListener((View.OnClickListener) this);

        }
        @Override
        public void onClick(View v) {
            if(itemClickListener != null){
                itemClickListener.onClick(v, getAdapterPosition());
            }
        }


    }

    public void itemRemove(int position,int productid) {
        DbHelper myDb = new DbHelper(mContext);
      //  position = position-1;
        notifyItemRemoved(position);
        this.notifyDataSetChanged();
      //  position = position+1;
        myDb.deleteproduct(productid);
    }

}
