package com.example.mystore.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;


import com.example.mystore.BillActivity;
import com.example.mystore.Dateformatchange;


import com.example.mystore.ProductsRecycleClick;
import com.example.mystore.R;

import com.example.mystore.ReportActivity;
import com.example.mystore.db.DbHelper;
import com.example.mystore.model.Bill;


import java.util.ArrayList;
import java.util.List;

public class BillAdapter extends RecyclerView.Adapter <BillAdapter.BillViewHolder>{


    private Context mContext;
    List<Bill> mData;
    List<Bill>lstbill;
    Dialog  myDialog;
    DbHelper myDb;
    RecyclerView billrec;
    static TextView totam;
    static TextView totqty1;
    BillRecAdapter rcAdaptor;
    //   private BillRecycleClick itemClickListener;

    private static ProductsRecycleClick itemClickListener;

    public BillAdapter(Context context, List<Bill> cursor) {
        mContext = context;
        mData = cursor;
    }

    @NonNull
    @Override
    public BillViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.bill_report_rc, parent, false);
        final BillViewHolder viewHolder = new BillViewHolder(view);

        myDialog = new Dialog(mContext);
        myDialog.setContentView(R.layout.billdata);

        myDb = new DbHelper(mContext);
        totqty1 = myDialog.findViewById(R.id.tvtotalqty);
        totam = myDialog.findViewById(R.id.tvtotalamnt);

        billrec = (RecyclerView) myDialog.findViewById(R.id.billdatarecycleview);




        return new BillViewHolder(view);


    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final BillViewHolder holder, final int position) {

        final int  orderid = mData.get(position).getOrderid();

        final String date = mData.get(position).getDate();
        final String amt = mData.get(position).getTotal();

      holder.amttv.setText(amt);
      holder.Billnotv.setText(String.valueOf( orderid));
      holder.datetv.setText(Dateformatchange.Dchange(date));
      holder.rc_Bill.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {



              int ordersid = mData.get(position).getOrderid();
              Intent intent = new Intent(mContext, BillActivity.class);
              intent.putExtra("ordersid", ordersid);


             mContext.startActivity(intent);

              ReportActivity re = new ReportActivity();
              re.finish();

          }
      });



    }



    public void filterList(ArrayList<Bill> filteredList) {
        mData = filteredList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void  setClickListener(ProductsRecycleClick itemClickListener){


        this.itemClickListener = itemClickListener ;

    }

    public static class BillViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {



        private ConstraintLayout rc_Bill;

        private TextView Billnotv;
        private TextView amttv;
        private TextView datetv;
        //   private TextView datetv;


        public BillViewHolder(@NonNull View itemView) {
            super(itemView);
            Billnotv = itemView.findViewById(R.id.tvbillno);
            amttv = itemView.findViewById(R.id.tvprice);
            datetv = itemView.findViewById(R.id.tvdate);
            rc_Bill = itemView.findViewById(R.id.rcviewbill);

              itemView.setOnClickListener((View.OnClickListener) this);

        }
        @Override
        public void onClick(View v) {
            if(itemClickListener != null){
                itemClickListener.onClick(v, getAdapterPosition());
            }
        }


    }



}
