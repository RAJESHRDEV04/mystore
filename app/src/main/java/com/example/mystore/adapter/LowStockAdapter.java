package com.example.mystore.adapter;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;


import com.example.mystore.BillActivity;
import com.example.mystore.Dateformatchange;
import com.example.mystore.Imageconverter;

import com.example.mystore.ProductsRecycleClick;
import com.example.mystore.R;
import com.example.mystore.ReportActivity;
import com.example.mystore.SaleActivity;
import com.example.mystore.db.DbHelper;
import com.example.mystore.model.Bill;
import com.example.mystore.model.Products;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;



public class LowStockAdapter extends RecyclerView.Adapter <LowStockAdapter.LowStockViewHolder>{

    private Context mContext;
    List<Products> mData;

    DbHelper myDb;

    private static ProductsRecycleClick itemClickListener;

    public LowStockAdapter(Context context, List<Products> cursor) {
        mContext = context;
        mData = cursor;
    }

    @NonNull
    @Override
    public LowStockViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.lowstock_rcview, parent, false);
        final LowStockViewHolder viewHolder = new LowStockViewHolder(view);




        return new LowStockViewHolder(view);

    }




    @RequiresApi(api = Build.VERSION_CODES.O)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final LowStockViewHolder holder, final int position) {

        final int  productid = mData.get(position).getProductid();
        final String productname =  mData.get(position).getProductname();

        // final String date = mData.get(position).getDate();


        final String qty1 = mData.get(position).getQuantity();

        if(mData.size()!=0){
            holder.nametv.setText(productname);
            if(Integer.parseInt(qty1) == 0 ){
               holder.qtytv.setText("No Stock");
               holder.qtytv.setTextColor(Color.RED);
            }else {
                holder.qtytv.setText(qty1);
            }

        }


    }




    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void  setClickListener(ProductsRecycleClick itemClickListener){
        this.itemClickListener = itemClickListener ;

    }

public static class LowStockViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {



    private final ConstraintLayout rc_LowStock;

    private final TextView nametv;

    private final TextView qtytv;
    //   private TextView datetv;
    private  Button btnadd;


    public LowStockViewHolder(@NonNull View itemView) {
        super(itemView);
        nametv = itemView.findViewById(R.id.tvlwstockproname);

        qtytv = itemView.findViewById(R.id.tvlwstockqty);


        rc_LowStock = itemView.findViewById(R.id.rcview);

        //  itemView.setOnClickListener((View.OnClickListener) this);

    }
    @Override
    public void onClick(View v) {
        if(itemClickListener != null){
            itemClickListener.onClick(v, getAdapterPosition());
        }
    }


}


}
