package com.example.mystore.adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mystore.AddCustomersActivity;
import com.example.mystore.ProductsRecycleClick;
import com.example.mystore.R;
import com.example.mystore.db.DbHelper;
import com.example.mystore.model.Customer;

import java.util.ArrayList;
import java.util.List;

public class CustomerAdapter extends RecyclerView.Adapter <CustomerAdapter.ContactViewHolder>{

    private Context mContext;
    List<Customer> mData;
    String mobile1;
    private static final int REQUEST_CALL = 1;

    private static ProductsRecycleClick itemClickListener;

    public CustomerAdapter(Context context, List<Customer> cursor) {
        mContext = context;
        mData = cursor;
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.contact_rc, parent, false);
        final ContactViewHolder viewHolder = new ContactViewHolder(view);


        return new ContactViewHolder(view);

    }


    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, final int position) {

        final String name =  mData.get(position).getCustomername();

        String firstLetter = name.substring(0,1);
        firstLetter = firstLetter.toUpperCase();

        holder.nametv.setText(name);
        holder.fl.setText(firstLetter);


        final int  conid = mData.get(position).getCid();


        final String mobile = mData.get(position).getMobile();
        mobile1 = mobile;
        final String address = mData.get(position).getAddress();
        final String email = mData.get(position).getEmail();



        holder.rc_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makePhoneCall();
            }
        });


        holder.rc_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, AddCustomersActivity.class);

                intent.putExtra("conid", conid);
                intent.putExtra("name", name);
                intent.putExtra("mobile", mobile);
                intent.putExtra("address", address);
                intent.putExtra("email", email);

                mContext.startActivity(intent);

            }
        });
    }



    @Override
    public int getItemCount() {

        return mData.size();
    }

    public void filterList(ArrayList<Customer> filteredList) {
        mData = filteredList;
        notifyDataSetChanged();
    }


    public void  setClickListener(ProductsRecycleClick itemClickListener){
        this.itemClickListener = itemClickListener ;

    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {

        private ImageView rc_edit;
        private ImageView rc_call;
        private TextView nametv;
        private TextView fl;

        private TextView intresttv;

        public ContactViewHolder(@NonNull View itemView) {
            super(itemView);
            nametv = itemView.findViewById(R.id.tvrcname);
            fl = itemView.findViewById(R.id.tvrcfl);
            rc_edit = itemView.findViewById(R.id.imgrceditbtn);
            rc_call = itemView.findViewById(R.id.imgrccallbtn);

        }


        @Override
        public void onClick(View v) {
            if(itemClickListener != null){
                itemClickListener.onClick(v, getAdapterPosition());
            }

        }
    }

    public void itemRemoved(int position) {
        DbHelper myDb = new DbHelper(mContext);

        position = position-1;
        notifyItemRemoved(position);
        this.notifyDataSetChanged();
        position = position+1;
        myDb.deleteContact(position);
    }


    private void makePhoneCall() {
        int number = Integer.parseInt(mobile1);

        if (ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) mContext,
                    new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL);
        } else {
            String dial = "tel:" + mobile1;
            mContext.startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
        }

    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CALL) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                makePhoneCall();
            } else {
                Toast.makeText(mContext, "Permission DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }

}

