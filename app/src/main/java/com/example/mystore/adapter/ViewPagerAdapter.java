package com.example.mystore.adapter;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.mystore.fragment.FragmentProducts;
import com.example.mystore.model.Products;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private final List<Fragment> lstFragment = new ArrayList<>();
    private final List<String> lstTitles = new ArrayList<String>();

    public ViewPagerAdapter( FragmentManager fm) {
        super(fm);
    }



    @NonNull
    @Override
    public Fragment getItem(int position) {
        return lstFragment.get(position);
    }

    @Override
    public int getCount() {
        return lstTitles.size();

    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return (CharSequence) lstTitles.get(position);
    }

    public void AddFragment (Fragment fragment, String title) {
        lstFragment.add(fragment);
        lstTitles.add(title);
    }

    public void destroyItem(FragmentProducts fragmentProducts, List<Products> lstProducts) {
        lstFragment.clear();
    }

    public void replaceFragment(int index, Fragment fragment, String title) {
        lstFragment.set(index, fragment);
        lstTitles.add(index, title);
    }
}

