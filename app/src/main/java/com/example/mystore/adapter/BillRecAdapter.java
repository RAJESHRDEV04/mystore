package com.example.mystore.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.mystore.Dateformatchange;
import com.example.mystore.Imageconverter;


import com.example.mystore.ProductsRecycleClick;
import com.example.mystore.R;

import com.example.mystore.fragment.FragmentBillReport;
import com.example.mystore.model.Bill;


import java.util.List;

public class BillRecAdapter extends RecyclerView.Adapter <BillRecAdapter.BillViewHolder>{

    private Context mContext;
     public List<Bill> mData;
   // Dialog  myDialog;
    //   private BillRecycleClick itemClickListener;

    private static ProductsRecycleClick itemClickListener;

    public BillRecAdapter(Context context, List<Bill> cursor) {
        mContext = context;
        mData = cursor;
    }

    public BillRecAdapter() {

    }

    @NonNull
    @Override
    public BillViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.billdata_rcview, parent, false);
        final BillViewHolder viewHolder = new BillViewHolder(view);

       //  myDialog = new Dialog(mContext);
       //  myDialog.setContentView(R.layout.billdata);

        getbilltotal();
        return new BillViewHolder(view);


    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull BillViewHolder holder, final int position) {

      //  final int  orderid = mData.get(position).getOrderid();

        final String date = mData.get(position).getDate();
        final String amt = mData.get(position).getTotal();
        final String pname = mData.get(position).getProductname();
        final String qty = mData.get(position).getQty();
        final String rate = mData.get(position).getRate();
        holder.pnametv.setText(pname);
        holder.amttv.setText(amt);


        if(Integer.parseInt(qty) <10){
            holder.qtytv.setText(""+ qty);
        }else {
            holder.qtytv.setText(qty);
        }

        holder.ratetv.setText(rate);




    }



    private Drawable showimage(String productimage) {

        Imageconverter  imageconverter = new Imageconverter();

        Bitmap image = imageconverter.stringToBitmap(productimage);
        Drawable d = new BitmapDrawable(mContext.getResources(), image);
        return  d;
    }


    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void  setClickListener(ProductsRecycleClick itemClickListener){


        this.itemClickListener = itemClickListener ;

    }

    public void deleteItem(int adapterPosition) {
        mData.remove(adapterPosition);
    }

    public static class BillViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {



        private ConstraintLayout rc_Bill;

        private TextView Billnotv;
        private TextView amttv;
        private TextView datetv;
        private TextView pnametv;
        private TextView qtytv;
        private TextView ratetv;


        public BillViewHolder(@NonNull View itemView) {
            super(itemView);

            amttv = itemView.findViewById(R.id.tvamnt);
            qtytv = itemView.findViewById(R.id.tvquantity);
            ratetv = itemView.findViewById(R.id.tvrate);
            pnametv = itemView.findViewById(R.id.tvproductname);
            rc_Bill = itemView.findViewById(R.id.recyviewbill);

            //  itemView.setOnClickListener((View.OnClickListener) this);

        }
        @Override
        public void onClick(View v) {
            if(itemClickListener != null){
                itemClickListener.onClick(v, getAdapterPosition());
            }
        }


    }


      void getbilltotal() {

          int totqty = 0;
          int tot = 0;
          for (int i = 0; i < mData.size(); i++) {
              tot = tot + Integer.parseInt(mData.get(i).getTotal());
              totqty = totqty + Integer.parseInt(mData.get(i).getQty());

              BillAdapter.totqty1.setText(String.valueOf(totqty));
              BillAdapter.totam.setText(String.valueOf(tot));
          }

      }


}
