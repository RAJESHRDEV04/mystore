package com.example.mystore;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class BarcodeScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler
{
 ZXingScannerView Scanerview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Scanerview = new ZXingScannerView(this);
        setContentView(Scanerview);


    }

    @Override
    public void handleResult(Result result) {

        AddproductsActivity.edbarcode.setText(result.getText());
        onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Scanerview.stopCamera();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Scanerview.setResultHandler(this);
        Scanerview.startCamera();
    }
}