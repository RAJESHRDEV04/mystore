package com.example.mystore;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mystore.db.DbHelper;
import com.example.mystore.model.Products;
import com.google.android.material.textfield.TextInputEditText;


import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AddproductsActivity extends AppCompatActivity {

    private static final int GALLERY_REQUEST_CODE = 100;
    private static final int CAMERA_REQUEST_CODE = 200;

    Boolean imgchange = false;

    Button savebtn,addbtn,subbtn,scanbtn,clearbtn;
    EditText edquantity;
    @SuppressLint("StaticFieldLeak")
    public static EditText edbarcode;
    AutoCompleteTextView edproductname,Brandname,Category;
    ImageView btnhome,imgproduct;
    TextInputEditText edbuyingprice,edsellingprice, edproductdetails;
    DbHelper myDb;
    TextView tvaddproducts;
    String Bname,Pname,Cat;
    EditText tvdate;
    int Bid,Pid;
    int year,month,day;
    List<Products> products;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addproducts);

       ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, PackageManager.PERMISSION_GRANTED);

       products = new ArrayList<Products>();
        savebtn = findViewById(R.id.btnsave);
        tvaddproducts = findViewById(R.id.tvaddproducts);
        scanbtn=findViewById(R.id.btnscan);
        clearbtn=findViewById(R.id.btnclear);
        addbtn=findViewById(R.id.btnadd);
        subbtn=findViewById(R.id.btnsub);
        tvdate=findViewById(R.id.tvdate);
        btnhome=findViewById(R.id.btnhome);
        edbuyingprice=findViewById(R.id.edbuyingprice);
        edsellingprice=findViewById(R.id.edsellingprice);
        edproductname = findViewById(R.id.edproductname);
        edproductdetails = findViewById(R.id.edproductdetails);
        imgproduct = findViewById(R.id.imgproduct);
        edbarcode = findViewById(R.id.edbarcodeno);
        edquantity = findViewById(R.id.edquantity);
        myDb = new DbHelper(this);
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        tvdate.setText(Dateformatchange.Dchange(parsePickerDate(year,month+1,day)));

        clearbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Clr();
            }
        });

        btnhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddproductsActivity.this,MainActivity.class);
                startActivity(intent);

            }
        });
        final List<String> Bnames = new ArrayList<>();
        Cursor cursor = myDb.getBrandName();
        if (cursor != null){
            while(cursor.moveToNext()){
                Bnames.add(cursor.getString(cursor.getColumnIndex("BRANDNAME")));
            }
            cursor.close();
        }

         Brandname = findViewById(R.id.edbrandname);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, Bnames);
        Brandname.setAdapter(adapter);
      //  Brandname.setThreshold(2);
         Bname = String.valueOf(Brandname.getText());

        final List<String> Pnames = new ArrayList<>();
        Cursor cursor3 = myDb.getProductName();
        if (cursor3 != null){
            while(cursor3.moveToNext()){
                Pnames.add(cursor3.getString(cursor3.getColumnIndex("PRODUCTNAME")));
            }
            cursor3.close();
        }


        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, Pnames);
        edproductname.setAdapter(adapter3);
        //  Brandname.setThreshold(2);
        Pname = String.valueOf(edproductname.getText());
        if(Pname =="All"){
            Pname ="";
        }

        final List<String> Cats = new ArrayList<>();
        Cursor cursor1 = myDb.getCategories();
        if (cursor1 != null){
            while(cursor1.moveToNext()){
                Cats.add(cursor1.getString(cursor1.getColumnIndex("CATEGORIESNAME")));
            }
            cursor1.close();
        }

         Category = findViewById(R.id.edcategory);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, Cats);

        Category.setAdapter(adapter1);
        Cat = String.valueOf(Category.getText());

        getIncomingIntent();
       scanbtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               ScanBarcode(v);
           }
       });
        tvdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        AddproductsActivity.this,R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        month = month+1;
                        //String date = day+"/"+month+"/"+year;

                        String date = parsePickerDate(year,month,day);
                        tvdate.setText(Dateformatchange.Dchange(date));

                    }
                },year,month,day);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();

            }
        });


        savebtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        String productname = edproductname.getText().toString().trim();
                        String productdetails = edproductdetails.getText().toString().trim();
                        String barcode = edbarcode.getText().toString().trim();
                        String bna = Brandname.getText().toString().trim();
                        String catt = Category.getText().toString().trim();
                        String buyprice = edbuyingprice.getText().toString().trim();
                        String sellprice = edsellingprice.getText().toString();
                        String qty = edquantity.getText().toString();
                        Bitmap image = ((BitmapDrawable) imgproduct.getDrawable()).getBitmap();
                        String productimage;
                        if(imgchange){

                             productimage = Imageconverter.bitmapToString(Imageconverter.resizeBitmap(image));
                        }else {
                             productimage = Imageconverter.bitmapToString(image);

                        }


                        String date = Dateformatchange.Ychange(tvdate.getText().toString());
                        Integer catid = myDb.getCid(catt);

                        Integer bid = myDb.getCid(catt,bna);

                        if(catid == -1){
                            if(!catt.isEmpty()&& !bna.isEmpty()){
                                boolean Inserted = myDb.insertCategories(catt,bna);
                                if(Inserted){
                                    Toast.makeText(AddproductsActivity.this, "Categories Added", Toast.LENGTH_LONG).show();


                                }else{
                                    Toast.makeText(AddproductsActivity.this, "Categories Not Added", Toast.LENGTH_LONG).show();

                                }
                            }

                        }
                        if(bid == -1){
                            if(!catt.isEmpty()&& !bna.isEmpty()){
                                boolean Inserted = myDb.insertCategories(catt,bna);
                                if(Inserted){
                                    Toast.makeText(AddproductsActivity.this, "Categories Added", Toast.LENGTH_LONG).show();


                                }else{
                                    Toast.makeText(AddproductsActivity.this, "Categories Not Added", Toast.LENGTH_LONG).show();

                                }
                            }
                        }



                        catid = myDb.getCid(catt,bna);
                        if (savebtn.getText() == "UPDATE") {


                            if (!TextUtils.isEmpty(productname) && !TextUtils.isEmpty(bna)&& !TextUtils.isEmpty(catt)  && !TextUtils.isEmpty(buyprice)  && !TextUtils.isEmpty(sellprice) && !TextUtils.isEmpty(productdetails) && !TextUtils.isEmpty(barcode) ) {


                                 boolean isInserted = myDb.updateProducts(Pid, productname,catid, productdetails,buyprice,sellprice,Integer.parseInt(qty), barcode,productimage,date);

                                    if (isInserted) {

                                        Toast.makeText(AddproductsActivity.this, "Product UPDATED", Toast.LENGTH_LONG).show();
                                         Intent intent = new Intent(AddproductsActivity.this, ProductsActivity.class);
                                             startActivity(intent);
                                             finish();
                                    } else
                                    {
                                        Toast.makeText(AddproductsActivity.this, "Product NOT UPDATED", Toast.LENGTH_LONG).show();
                                }
                            } else {
                               Toast.makeText(AddproductsActivity.this, "ENTER ALL DATA", Toast.LENGTH_LONG).show();

                           }

                        } else {

                         if (!TextUtils.isEmpty(productname) && !TextUtils.isEmpty(bna)&& !TextUtils.isEmpty(catt)  && !TextUtils.isEmpty(buyprice)  && !TextUtils.isEmpty(sellprice) && !TextUtils.isEmpty(productdetails) && !TextUtils.isEmpty(barcode)) {


                                    int isInserted = myDb.insertProductData( productname,catid, productdetails,buyprice,sellprice, barcode,productimage,date);

                                    if (isInserted>0) {

                                         int sid =  myDb.insertStock(isInserted, date,qty);
                                            myDb.updateProductsStock(isInserted,sid);

                                        Toast.makeText(AddproductsActivity.this, "PRODUCT ADDED", Toast.LENGTH_LONG).show();
                                        Clr();
                                    } else
                                        Toast.makeText(AddproductsActivity.this, "PRODUCT NOT INSERTED", Toast.LENGTH_LONG).show();

                            } else {
                                Toast.makeText(AddproductsActivity.this, "ENTER ALL DATA", Toast.LENGTH_LONG).show();

                            }
                        }
                    }
                }
        );



       addbtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
              int pqty =  Integer.parseInt(edquantity.getText().toString());
              pqty = pqty+1;
               if((pqty)<10){
                   edquantity.setText("0"+pqty);
               }else {
                   edquantity.setText(""+pqty);
               }           }
       });

        subbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int pqty =  Integer.parseInt(edquantity.getText().toString());
                if(pqty >1 )
                {
                pqty = pqty-1;
                    if((pqty)<10){
                        edquantity.setText("0"+pqty);
                    }else {
                        edquantity.setText(""+pqty);
                    }                  }
            }
        });

    }


    public void getIncomingIntent(){

        if(getIntent().hasExtra("pid") && getIntent().hasExtra("pname") && getIntent().hasExtra("brand")  && getIntent().hasExtra("cat")&& getIntent().hasExtra("qty")&& getIntent().hasExtra("sellprice") ){


             Pid = getIntent().getIntExtra("pid",0);





            String Name = getIntent().getStringExtra("pname");
            String Cat = getIntent().getStringExtra("cat");
            String brand = getIntent().getStringExtra("brand");
            String qty = getIntent().getStringExtra("qty");
            String buyprice = getIntent().getStringExtra("buyprice");
            String sellprice = getIntent().getStringExtra("sellprice");
            String barcode = getIntent().getStringExtra("barcode");
            String productimage  = getIntent().getStringExtra("pimage");
            String prdesc = getIntent().getStringExtra("pdesc");
            String date = getIntent().getStringExtra("date");

            edproductname.setText(Name);
            Category.setText(Cat);
            Brandname.setText(brand);
            edquantity.setText(qty);
            edbuyingprice.setText(buyprice);
            edsellingprice.setText(sellprice);
            edbarcode.setText(barcode);
            edproductdetails.setText(prdesc);
            tvdate.setText(Dateformatchange.Dchange(date));
            imgproduct.setImageDrawable(showimage(productimage));

            savebtn.setText("UPDATE");


        }
    }


    private Drawable showimage(String productimage) {

        Imageconverter  imageconverter = new Imageconverter();

        Bitmap image = imageconverter.stringToBitmap(productimage);
        Drawable d = new BitmapDrawable(getResources(), image);
        return  d;
    }

    private void Clr() {

        edproductname.setText("");
        edproductdetails.setText("");
        Brandname.setText("");
        Category.setText("");
        edbarcode.setText("");
        edbuyingprice.setText("");
        edsellingprice.setText("");

        int imageResource = getResources().getIdentifier("@drawable/proimg", null, getPackageName());

        Drawable res = getResources().getDrawable(imageResource);

        imgproduct.setImageDrawable(res);
        edquantity.setText("01");
        tvdate.setText(Dateformatchange.Dchange( parsePickerDate(year,month+1,day)));
    }


    public void ScanBarcode(View view) {

     //   IntentIntegrator intentIntegrator = new IntentIntegrator(this);
      //  intentIntegrator.initiateScan();
startActivity(new Intent(getApplicationContext(),BarcodeScanActivity.class));
    }

    public void openGallery(View view) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_REQUEST_CODE);
    }

    public void openCamera(View view) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        imgchange = true;

        if (resultCode == RESULT_OK && requestCode == GALLERY_REQUEST_CODE) {
            try {
                Uri selectedImage = data.getData();
                InputStream imageStream = getContentResolver().openInputStream(selectedImage);
              //  imageStream.siz
               // if( Integer.parseInt( BitmapFactory.decodeStream(imageStream)) > 1000 )
                imgproduct.setImageBitmap(BitmapFactory.decodeStream(imageStream));

            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }

        if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imgproduct.setImageBitmap(imageBitmap);
        }
    }
    public static String parsePickerDate(int Year, int Month, int day) {

        String sYear = String.valueOf(Year);
        String sMonth = String.valueOf((Month));
        String sDay = String.valueOf(day);

        if (sDay.length() == 1)
            sDay = "0" + sDay;

        if (sMonth.length() == 1)
            sMonth = "0" + sMonth;

        if (sDay.length() == 1)
            sDay = "0" + sDay;

        return sYear + "-" + sMonth + "-" + sDay;

    }
}