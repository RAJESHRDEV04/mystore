package com.example.mystore.ui.main;

import androidx.lifecycle.ViewModelProviders;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.mystore.BillActivity;
import com.example.mystore.R;



public class BillViewFragment extends Fragment {

    View v;
    private BillViewModel mViewModel;
    public static TextView tvtotal,tvqty;

    public static BillViewFragment newInstance() {
        return new BillViewFragment();
    }

    @Nullable
    @Override

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.bill_view_fragment, container, false);
        tvtotal = v.findViewById(R.id.tvtotalamt);
        tvqty = v.findViewById(R.id.tvtotalqty);

        gettotall();
       return v;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(BillViewModel.class);
        // TODO: Use the ViewModel



    }
    @SuppressLint("SetTextI18n")
    public void gettotall() {

        int tot = 0;
        int qtytot =0;
 
        int it = 0;
        for (int i = 0; i < BillActivity.listbill.size(); i++) {

            View view = BillActivity.recyclerviewbill.getChildAt(i);

            if(view != null) {
                TextView pt = (TextView) view.findViewById(R.id.tvamnt);
                EditText qt = (EditText) view.findViewById(R.id.tvquantity);
                tot = tot + Integer.parseInt(pt.getText().toString());
                qtytot = qtytot + Integer.parseInt(qt.getText().toString());
            }
        }

        tvtotal.setText(String.valueOf(tot));
        if(qtytot<10){
            tvqty.setText("0"+qtytot);
        }else {
            tvqty.setText(String.valueOf(qtytot));
        }

    }


}