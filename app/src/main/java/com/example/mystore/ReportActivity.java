package com.example.mystore;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mystore.adapter.ViewPagerAdapter;
import com.example.mystore.db.DbHelper;
import com.example.mystore.fragment.FragmentBillReport;
import com.example.mystore.fragment.FragmentProductReport;
import com.example.mystore.fragment.FragmentSearchProducts;
import com.example.mystore.model.Bill;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class ReportActivity extends AppCompatActivity {
    TextView tvfilter;
    ImageView btnhome;
    ViewPager viewpgrreport;
    EditText edstartdate,edenddate;
   // Spinner spinrfilter;
    Button btnview,btnrefresh;
    RadioGroup rdgrpreport;
    RadioButton rdbill,rdproducts;
    int year,month,day;
    String startdate,enddate;
    DbHelper myDb;
    public static List<Bill> listbill,listbillpd;
    ViewPagerAdapter viewPagerAdapter;

    int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE}, PackageManager.PERMISSION_GRANTED);
        myDb = new DbHelper(this);

        btnhome = findViewById(R.id.btnhome);
        btnrefresh = findViewById(R.id.btnrefresh);
        viewpgrreport = findViewById(R.id.viewpgrreport);
        edenddate = findViewById(R.id.edenddate);
        edstartdate = findViewById(R.id.edstartdate);
        //spinrcategory= findViewById(R.id.spinrcategory);
       // spinrfilter = findViewById(R.id.spinrfilter);
        //btnpdf = findViewById(R.id.btnpdf);
       // tvfilter = findViewById(R.id.tvfilter);
        btnview = findViewById(R.id.btnview);
        rdgrpreport = findViewById(R.id.rdgrpreport);
        rdbill = findViewById(R.id.rdbill);
        rdproducts = findViewById(R.id.rdproducts);

        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);


       // spinrfilter.setVisibility(View.GONE);
       // tvfilter.setVisibility(View.GONE);

        listbill = myDb.getAllbill();
        listbillpd = myDb.getbillproduct();
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.AddFragment(new FragmentBillReport(),"Report");

        viewpgrreport.setAdapter(viewPagerAdapter);

        btnrefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, 0);
                startActivity(getIntent());
               // overridePendingTransition(0, 0);
            }
        });

        btnhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReportActivity.this,MainActivity.class);
                startActivity(intent);

            }
        });

        edstartdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        ReportActivity.this,R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        month = month+1;
                        //String date = day+"/"+month+"/"+year;

                         startdate = Datepick.parsePickerDate(year,month,day);
                        edstartdate.setText(Dateformatchange.Dchange(startdate));

                    }
                },year,month,day);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });
        edenddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        ReportActivity.this,R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        month = month+1;
                        //String date = day+"/"+month+"/"+year;

                         enddate = Datepick.parsePickerDate(year,month,day);

                        if(startdate == null){
                            Toast.makeText(ReportActivity.this, "Enter Start Date!!", Toast.LENGTH_LONG).show();

                        }else {
                            edenddate.setText(Dateformatchange.Dchange( enddate));

                        }
                     //   edstartdate.setText(date);

                    }
                },year,month,day);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });



        btnview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(enddate == null){
                    Toast.makeText(ReportActivity.this, "Enter Start Date and End Date !!! ", Toast.LENGTH_LONG).show();

                }else {

                    int radioButtonID = rdgrpreport.getCheckedRadioButtonId();

                    RadioButton radioButton = (RadioButton) rdgrpreport.findViewById(radioButtonID);

                    String selectedText = (String) radioButton.getText();

                    if(selectedText == "Bill"){


                         listbill.clear();
                         listbill = myDb.getbillsearch(startdate,enddate);
                   // spinrfilter.setVisibility(View.GONE);
                   // tvfilter.setVisibility(View.GONE);

                    viewPagerAdapter.AddFragment(new FragmentBillReport(),"lstProducts");

                    viewPagerAdapter.replaceFragment(0, new FragmentBillReport(),"lstProducts");
                    viewPagerAdapter.notifyDataSetChanged();

                    viewPagerAdapter.AddFragment(new FragmentBillReport(),"lstProducts");

                    viewpgrreport.setAdapter(viewPagerAdapter);
                }else{
                        listbillpd.clear();
                        listbillpd = myDb.getbillproduct(startdate,enddate);
                        viewPagerAdapter.AddFragment(new FragmentProductReport(),"lstProducts");

                        viewPagerAdapter.replaceFragment(0, new FragmentProductReport(),"lstProducts");
                        // viewPagerAdapter.notifyDataSetChanged();

                        viewPagerAdapter.AddFragment(new FragmentProductReport(),"lstProducts");

                        viewpgrreport.setAdapter(viewPagerAdapter);
                    }
                }

            }
        });


        rdgrpreport.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // int selectedId = rdbkch.getCheckedRadioButtonId();
                View radioButton = rdgrpreport.findViewById(checkedId);

              //
                 index = rdgrpreport.indexOfChild(radioButton);

                if(index == 1){
                   // spinrfilter.setVisibility(View.VISIBLE);
                   // tvfilter.setVisibility(View.VISIBLE);
                    viewPagerAdapter.AddFragment(new FragmentProductReport(),"lstProducts");

                    viewPagerAdapter.replaceFragment(0, new FragmentProductReport(),"lstProducts");
                   // viewPagerAdapter.notifyDataSetChanged();

                    viewPagerAdapter.AddFragment(new FragmentProductReport(),"lstProducts");

                    viewpgrreport.setAdapter(viewPagerAdapter);

                }else {


                   // spinrfilter.setVisibility(View.GONE);
                   // tvfilter.setVisibility(View.GONE);

                    viewPagerAdapter.AddFragment(new FragmentBillReport(),"lstProducts");

                    viewPagerAdapter.replaceFragment(0, new FragmentBillReport(),"lstProducts");
                    viewPagerAdapter.notifyDataSetChanged();

                    viewPagerAdapter.AddFragment(new FragmentBillReport(),"lstProducts");

                    viewpgrreport.setAdapter(viewPagerAdapter);
                }

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void printInvoice(){
        PdfDocument pdfDocument = new PdfDocument();
        Paint paint = new Paint();
        String[] columns = {"Invoiceno,ProductDescription,date,qty,amount"};

        PdfDocument.PageInfo pageInfo = new  PdfDocument.PageInfo.Builder(595,842,1).create();

        PdfDocument.Page page = pdfDocument.startPage(pageInfo);
        Canvas canvas = page.getCanvas();

        paint.setTextSize(20);
        canvas.drawText("My Store ",30,80,paint);

        paint.setTextSize(16);
        canvas.drawText("IV KUZARI PUT, ZAGREB 10000",30,130,paint);

        paint.setTextAlign(Paint.Align.RIGHT);
        canvas.drawText("Invoice No :",canvas.getWidth()-40,40,paint);
        canvas.drawText("12345",canvas.getWidth()-40,80,paint);

        paint.setTextAlign(Paint.Align.RIGHT);
        paint.setColor(Color.rgb(150,150,150));
        canvas.drawRect(30,150,canvas.getWidth()-30,160,paint);

        paint.setColor(Color.BLACK);
        canvas.drawText("Date :",50,200,paint);

        canvas.drawText("14-10-2020",250,200,paint);

        canvas.drawText("Time : ",620,200,paint);
        paint.setTextAlign(Paint.Align.RIGHT);
        canvas.drawText("14-10-2020",canvas.getWidth()-40,200,paint);

        paint.setTextAlign(Paint.Align.RIGHT);

        paint.setColor(Color.rgb(150,150,150));
        canvas.drawRect(30,250,250,300,paint);

     //   paint.setColor(Color.WHITE);

        canvas.drawText("Product Description",50,435,paint);
        canvas.drawText("Qty",550,435,paint);
        paint.setTextAlign(Paint.Align.RIGHT);
        canvas.drawText("Amount",canvas.getWidth()-40,435,paint);

        paint.setTextAlign(Paint.Align.LEFT);


        pdfDocument.finishPage(page);

        String myFilePath = Environment.getExternalStorageDirectory().getPath() + "/myPDFFile12.pdf";
       //
        // File file = new File (this.getExternalFilesDir("/"),1+"myestore.pdf");

        File myFile = new File(myFilePath);
        try {
            pdfDocument.writeTo(new FileOutputStream(myFile));
            Toast.makeText(ReportActivity.this, "Categories Not Added", Toast.LENGTH_LONG).show();
        }
        catch (Exception e){
            e.printStackTrace();

        }
        pdfDocument.close();


    }


    public  void viewbill(int ordersid){

        finish();
    }
}