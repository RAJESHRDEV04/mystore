package com.example.mystore;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Dateformatchange {

    public static String Dchange(String date1){
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = (Date)formatter.parse(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String mydate = dateFormat.format(date);

        return mydate;

    }
    public static String Ychange(String date1){
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;
        try {
            date = (Date)formatter.parse(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String mydate = dateFormat.format(date);

        return mydate;

    }
}
