package com.example.mystore.fragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.mystore.R;
import com.example.mystore.ReportActivity;
import com.example.mystore.adapter.BillAdapter;
import com.example.mystore.adapter.ProductReportAdapter;
import com.example.mystore.adapter.ProductsAdapter;
import com.example.mystore.adapter.ViewPagerAdapter;
import com.example.mystore.db.DbHelper;
import com.example.mystore.model.Bill;
import com.example.mystore.model.Products;

import java.util.ArrayList;
import java.util.List;

public class FragmentProductReport extends Fragment {

    public RecyclerView BillAllRcView;
    View v;

    private List<Bill> lstbill;
    DbHelper myDb;
    private ViewPager viewPager ;
    private ViewPagerAdapter viewPagerAdapter ;
    ProductReportAdapter rcAdaptor;
    public TextView tvtotalpro,tvtotalamnt;



    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         v = inflater.inflate(R.layout.fragment_product_report, container, false);


        tvtotalpro = v.findViewById(R.id.tvtotalpro);
        tvtotalamnt = v.findViewById(R.id.tvtotalamnt);


        BillAllRcView = (RecyclerView) v.findViewById(R.id.rcproductreport);
        BillAllRcView.setHasFixedSize(true);

        rcAdaptor = new ProductReportAdapter(getContext(),lstbill);
        BillAllRcView.setLayoutManager(new LinearLayoutManager(getActivity()));

        BillAllRcView.setAdapter(rcAdaptor);
        EditText searchItem = v.findViewById(R.id.atvname);

        gettotall();

        searchItem.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }});


        searchItem.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s!=null){
                    filter(s.toString());
                }

            }
        });

        gettotall();


        return  v;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lstbill = new ArrayList<Bill>();
        myDb = new DbHelper(getActivity());
      //  lstbill= myDb.getbillproduct();
        lstbill = ReportActivity.listbillpd;


    }

    @SuppressLint("SetTextI18n")
    private void filter(String text) {
        int total = 0;
        ArrayList<Bill> filteredList = new ArrayList<>();
        if(!text.isEmpty()){
            for (Bill item : lstbill) {
                if (item.getProductname().toLowerCase().contains(text.toLowerCase())) {
                    filteredList.add(item);
                }
            }
            rcAdaptor.filterList(filteredList);


            for(int i = 0; i < filteredList.size(); i++){
                total += Integer.parseInt(filteredList.get(i).getTotal());
            }



            if(filteredList.size()<10){
                tvtotalpro.setText("0"+filteredList.size());

            }else {
                tvtotalpro.setText(String.valueOf(filteredList.size()));

            }            tvtotalamnt.setText(String.valueOf( total));

        }else{
            rcAdaptor.filterList((ArrayList<Bill>) lstbill);
            gettotall();
        }


    }
    public void gettotall() {


        int total = 0;
        for(int i = 0; i < lstbill.size(); i++){
            total += Integer.parseInt(lstbill.get(i).getTotal());
        }

        if(lstbill.size()<10){
            tvtotalpro.setText("0"+lstbill.size());

        }else {
            tvtotalpro.setText(String.valueOf(lstbill.size()));

        }
        tvtotalamnt.setText(String.valueOf( total));
    }
}