package com.example.mystore.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mystore.R;
import com.example.mystore.ReportActivity;
import com.example.mystore.SaleActivity;
import com.example.mystore.adapter.BillAdapter;
import com.example.mystore.db.DbHelper;
import com.example.mystore.model.Bill;

import java.util.ArrayList;
import java.util.List;

import static com.example.mystore.SaleActivity.SalesAllRcView;

public class FragmentTotal extends Fragment {


    public static TextView tvtotal;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_saletotal, container, false);


       tvtotal = v.findViewById(R.id.tvtotalamt);


        gettotall();

        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




    }
    public void gettotall() {

        int tot = 0;


        for (int i = 0; i < SaleActivity.lstProducts.size(); i++) {

            View view = SalesAllRcView.getChildAt(i);

       if(view != null) {
           TextView pt = (TextView) view.findViewById(R.id.amnt);

           tot = tot + Integer.parseInt(pt.getText().toString());
       }
        }
        tvtotal.setText(String.valueOf(tot));
    }


}
