package com.example.mystore.fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mystore.R;
import com.example.mystore.adapter.ProductsAdapter;
import com.example.mystore.db.DbHelper;
import com.example.mystore.model.Products;

import java.util.ArrayList;
import java.util.List;

public class FragmentProducts extends Fragment {

    View v;
    private RecyclerView ProductsAllRcView;
    private List<Products> lstProducts;
    DbHelper myDb;
    String selectcat,selectbr;

    public FragmentProducts() {

    }
    public FragmentProducts(String selcat,String selbr) {
     selectcat = selcat;
     selectbr = selbr;

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_products,container,false);

        setHasOptionsMenu(true);
        ProductsAllRcView = (RecyclerView) v.findViewById(R.id.recyclerView);
        ProductsAdapter productsAdaptor = new ProductsAdapter(getContext(),lstProducts);
        ProductsAllRcView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        ProductsAllRcView.setAdapter(productsAdaptor);
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        lstProducts = new ArrayList<Products>();
        myDb = new DbHelper(getActivity());
        lstProducts = myDb.getProductsbysearch();

    }

}
