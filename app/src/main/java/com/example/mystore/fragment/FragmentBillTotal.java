package com.example.mystore.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mystore.BillActivity;
import com.example.mystore.R;
import com.example.mystore.ReportActivity;
import com.example.mystore.SaleActivity;
import com.example.mystore.adapter.BillAdapter;
import com.example.mystore.db.DbHelper;
import com.example.mystore.model.Bill;
import com.example.mystore.ui.main.BillViewFragment;

import java.util.ArrayList;
import java.util.List;

import static com.example.mystore.BillActivity.recyclerviewbill;


public class FragmentBillTotal extends Fragment {

    View v;

    public static TextView tvtotal,tvqty;

    public static FragmentBillTotal newInstance() {
        return new FragmentBillTotal();
    }
    @SuppressLint("StaticFieldLeak")

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         v = inflater.inflate(R.layout.fragment_billtotal, container, false);


        tvtotal = v.findViewById(R.id.tvtotalamt);
        tvqty = v.findViewById(R.id.tvtotalqty);


        gettotall();

        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




    }
    public void gettotall() {

        int tot = 0;
        int qtytot =0;

        for (int i = 0; i < BillActivity.listbill.size(); i++) {

            View view = recyclerviewbill.getChildAt(i);

            if(view != null) {
                TextView pt = (TextView) view.findViewById(R.id.amnt);
                EditText qt = (EditText) view.findViewById(R.id.tvquantity);
                tot = tot + Integer.parseInt(pt.getText().toString());
                qtytot = qtytot + Integer.parseInt(qt.getText().toString());
            }
        }
        tvtotal.setText(String.valueOf(tot));
        tvqty.setText(String.valueOf(qtytot));
    }


}
