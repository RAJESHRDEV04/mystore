package com.example.mystore.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mystore.ProductsActivity;
import com.example.mystore.R;
import com.example.mystore.adapter.ProductsAdapter;
import com.example.mystore.db.DbHelper;
import com.example.mystore.model.Products;

import java.util.ArrayList;
import java.util.List;

public class FragmentSearchProducts extends Fragment {

    View v;
    private RecyclerView ProductsAllRcView;
    private List<Products> lstProducts;
    DbHelper myDb;

    public FragmentSearchProducts() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_products,container,false);

        setHasOptionsMenu(true);
        ProductsAllRcView = (RecyclerView) v.findViewById(R.id.recyclerView);
        ProductsAdapter productsAdaptor = new ProductsAdapter(getContext(),lstProducts);
        ProductsAllRcView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        ProductsAllRcView.setAdapter(productsAdaptor);
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        lstProducts = new ArrayList<Products>();
        myDb = new DbHelper(getActivity());

        String selectcat = null,selectbr = null;


       ProductsActivity activity = (ProductsActivity) getActivity();
        selectcat = activity.catspin.getSelectedItem().toString();
        selectbr = activity.brandspin.getSelectedItem().toString();

       if (!selectcat.equals("All") && selectbr.equals("All")) {


            lstProducts = myDb.getProductsbysearch(selectcat);
          }else if(selectcat.equals("All") && !selectbr.equals("All")){
           lstProducts.clear();
           lstProducts = myDb.getProductsbybrand(selectbr);

       }

       else if( !selectbr.equals("All")){
             lstProducts = myDb.getProductsbysearchBrand(selectcat, selectbr);
         }else {
           lstProducts = myDb.getProductsbysearch(selectcat);

       }
    }

}

