package com.example.mystore;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mystore.adapter.SalesAdapter;
import com.example.mystore.db.DbHelper;
import com.example.mystore.model.Products;
import com.google.zxing.Result;

import java.util.ArrayList;
import java.util.List;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static com.example.mystore.SaleActivity.SalesAllRcView;
import static com.example.mystore.SaleActivity.lstProducts;
import static com.example.mystore.SaleActivity.salesAdaptor;
import static com.example.mystore.SaleActivity.validateOrderProducts;

public class BarcodeScanOrderActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler
{

    DbHelper myDb;


    ZXingScannerView Scanerview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Scanerview = new ZXingScannerView(this);
        setContentView(Scanerview);


        myDb = new DbHelper(this);

    }

    @Override
    public void handleResult(Result result) {
        SaleActivity re = new SaleActivity();

        String barcode = result.getText();

        List<Products> lst = new ArrayList<>(myDb.getProductsbarcode(barcode));

        if (lst.isEmpty()) {
            Toast.makeText(BarcodeScanOrderActivity.this, "Out of stock", Toast.LENGTH_LONG).show();

        } else if (lstProducts.isEmpty()) {
            int lstqt = Integer.parseInt(lst.get(0).getQuantity());
            if(lstqt !=0) {
                lstProducts.addAll(lst);
                SalesAllRcView.setAdapter(salesAdaptor);
                salesAdaptor.setData(lstProducts);
            }else{
                Toast.makeText(BarcodeScanOrderActivity.this, "Out of stock", Toast.LENGTH_LONG).show();

            }
            //   lst.clear();
        } else if (validateOrderProducts(lst)) {
            int sqty = 0;
            int lstsize = lstProducts.size();
            for (int i = 0; i < lstsize; i++) {
                String lsbr = lstProducts.get(i).getBarcode();
                String lstbr = lst.get(0).getBarcode();
                List<Products> pr1 = new ArrayList<>();
                pr1 = lstProducts;

                if (lstProducts.get(i).getBarcode().equals(lst.get(0).getBarcode())) {


                    View view = SalesAllRcView.getChildAt(i);

                    EditText pq = (EditText) view.findViewById(R.id.edquantity);
                    sqty = sqty+ Integer.parseInt(pq.getText().toString());


                }
            }
            if ((Integer.parseInt(lst.get(0).getQuantity()) - sqty) > 0) {
                lstProducts.addAll(lst);
                // lst.clear();
            } else {
                Toast.makeText(BarcodeScanOrderActivity.this, "Out of stock ", Toast.LENGTH_LONG).show();

            }
        } else {
            lstProducts.addAll(lst);
            //  salesAdaptor.setData(lstProducts);


        }

        lst.clear();


        re.salereycle();

        onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Scanerview.stopCamera();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Scanerview.setResultHandler(this);
        Scanerview.startCamera();
    }
}