package com.example.mystore;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mystore.adapter.SalesAdapter;
import com.example.mystore.adapter.ViewPagerAdapter;
import com.example.mystore.db.DbHelper;
import com.example.mystore.fragment.FragmentProducts;
import com.example.mystore.fragment.FragmentTotal;
import com.example.mystore.model.Customer;
import com.example.mystore.model.Products;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.example.mystore.fragment.FragmentTotal.tvtotal;

public class SaleActivity extends AppCompatActivity {



    public static RecyclerView SalesAllRcView;
    public  static List<Products> lstProducts;
    public static  List<String> listtotal ;
    DbHelper myDb;
    ImageView scanbtn;
    Button btnadd,btnaddbr;
    Button btnpay ;
    Dialog myDialog;
    ImageView btnhome;
  //  Chip chipnew,chipold;
  //  ChipGroup chipGroup;
    AutoCompleteTextView Barcode,edmob;
    private String Brcode;
    int year,month,day;
    EditText billdate,edname,edemail,edaddress;

    private ViewPager viewPager ;
    // ProductsAdapter rc
    private ViewPagerAdapter viewPagerAdapter ;
    String date,edmobile;
    public static SalesAdapter salesAdaptor;
    public static TextView tvtotamt;
    List<Customer> lstCust;

    public  void salereycle() {


        SalesAllRcView.setAdapter(salesAdaptor);

        salesAdaptor.notifyDataSetChanged();

    }

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PackageManager.PERMISSION_GRANTED);

       ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, PackageManager.PERMISSION_GRANTED);

        btnadd=findViewById(R.id.btnadd);
        scanbtn =  findViewById(R.id.btnscan);
        tvtotamt =  findViewById(R.id.tvtotalamt);
        billdate = findViewById(R.id.edbilldate);


        edaddress = findViewById(R.id.edaddress);
        edemail = findViewById(R.id.edemail);
        edname = findViewById(R.id.edname);
       // chipGroup = findViewById(R.id.chipGroup);
        //chipnew = findViewById(R.id.chipnew);
       // chipold = findViewById(R.id.chipold);

        lstCust = new ArrayList<Customer>();

        btnhome=findViewById(R.id.btnhome);
        btnhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SaleActivity.this,MainActivity.class);
                startActivity(intent);

            }
        });
        lstProducts = new ArrayList<Products>();
        myDb = new DbHelper(this);
        myDialog = new Dialog(this);
        myDialog.setContentView(R.layout.addsale);

        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        date =  parsePickerDate(year,month+1,day);
        billdate.setText(Dateformatchange.Dchange(date));

        viewPager = (ViewPager) findViewById(R.id.viewtotalamnt);

        btnpay = findViewById(R.id.btnpay);

        final int totalamt = 0;

        SalesAllRcView = (RecyclerView) findViewById(R.id.rcviewsale);
        SalesAllRcView.setHasFixedSize(true);
        salesAdaptor = new SalesAdapter(this,lstProducts);
        SalesAllRcView.setLayoutManager(new LinearLayoutManager(this));
        SalesAllRcView.setAdapter(salesAdaptor);









        billdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        SaleActivity.this,R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        month = month+1;
                        //String date = day+"/"+month+"/"+year;

                        date = parsePickerDate(year,month,day);
                        billdate.setText(Dateformatchange.Dchange(date));

                    }
                },year,month,day);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });


      // gettotall();


        final Handler handler = new Handler();
        Timer timer = new Timer();
        TimerTask doTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @SuppressWarnings("unchecked")
                    public void run() {
                        try {
                            viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
                            viewPagerAdapter.AddFragment(new FragmentTotal(),"lstProducts");
                            viewPager.setAdapter(viewPagerAdapter);
                        }
                        catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });
            }
        };
        timer.schedule(doTask, 0, 10);





        btnaddbr = myDialog.findViewById(R.id.btnaddbr);



        final List<String> Bcode = new ArrayList<>();
        Cursor cursor1 = myDb.getAllProductsData();
        if (cursor1 != null){
            while(cursor1.moveToNext()){
                Bcode.add(cursor1.getString(cursor1.getColumnIndex("BARCODE")));
            }
            cursor1.close();
        }

        Barcode = myDialog.findViewById(R.id.atvname);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, Bcode);
        Barcode.setAdapter(adapter1);

        Brcode = String.valueOf(Barcode.getText());





          myDialog.findViewById(R.id.tvbillno);
        scanbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScanBarcode(v);
            }
        });

        btnaddbr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Brcode = String.valueOf(Barcode.getText());
                List<Products> lst = new ArrayList<>(myDb.getProductsbarcode(Brcode));

                //   labelbreak:
                if (lst.isEmpty()) {
                    Toast.makeText(SaleActivity.this, "Out of stock", Toast.LENGTH_LONG).show();

                } else if (lstProducts.isEmpty()) {
                    int lstqt = Integer.parseInt(lst.get(0).getQuantity());
                    if(lstqt !=0) {
                        lstProducts.addAll(lst);
                        SalesAllRcView.setAdapter(salesAdaptor);
                        salesAdaptor.setData(lstProducts);
                    }else{
                        Toast.makeText(SaleActivity.this, "Out of stock", Toast.LENGTH_LONG).show();

                    }
                    //   lst.clear();
                } else if (validateOrderProducts(lst)) {
                    int sqty = 0;
                    int lstsize = lstProducts.size();
                    for (int i = 0; i < lstsize; i++) {
                        String lsbr = lstProducts.get(i).getBarcode();
                        String lstbr = lst.get(0).getBarcode();
                        List<Products> pr1 = new ArrayList<>();
                        pr1 = lstProducts;

                        if (lstProducts.get(i).getBarcode().equals(lst.get(0).getBarcode())) {


                            View view = SalesAllRcView.getChildAt(i);

                            EditText pq = (EditText) view.findViewById(R.id.edquantity);
                            sqty = sqty+ Integer.parseInt(pq.getText().toString());


                        }
                    }
                    if ((Integer.parseInt(lst.get(0).getQuantity()) - sqty) > 0) {
                        lstProducts.addAll(lst);
                        // lst.clear();
                    } else {
                        Toast.makeText(SaleActivity.this, "Out of stock ", Toast.LENGTH_LONG).show();

                    }
                } else {
                    lstProducts.addAll(lst);
                  //  salesAdaptor.setData(lstProducts);


                }

                lst.clear();
                //  gettotall();
                salesAdaptor.setData(lstProducts);
                Barcode.setText("");
                myDialog.dismiss();


            }

        });



        final List<String> cmobile = new ArrayList<>();
        Cursor cursor = myDb.getMobileno();
        if (cursor != null){
            while(cursor.moveToNext()){
                cmobile.add(cursor.getString(cursor.getColumnIndex("MOBILE")));
            }
            cursor.close();
        }

        edmob = findViewById(R.id.edmob);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, cmobile);
        edmob.setThreshold(2);
        edmob.setAdapter(adapter);

        edmobile = String.valueOf(edmob.getText());


        edmob.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                edname.setText("");
                edemail.setText("");
                edaddress.setText("");

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                edmobile = String.valueOf(edmob.getText());


                lstCust = myDb.getcusomerbymob(edmobile);
                if(!lstCust.isEmpty()) {
                    edname.setText(lstCust.get(0).getCustomername());
                    edaddress.setText(lstCust.get(0).getAddress());
                    edemail.setText(lstCust.get(0).getEmail());


                }
            }
        });


        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.show();

            }
        });


        btnpay.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {

             if(!lstProducts.isEmpty())
             {
                 String ocmobile = edmob.getText().toString().trim();
                 String ocname = edname.getText().toString();
                 String ocaddress = edaddress.getText().toString();
                 String ocemail = edemail.getText().toString();


               if(ocmobile.length() != 10)  {
                   Toast.makeText(SaleActivity.this, "Enter valid mobile number", Toast.LENGTH_LONG).show();

               }
               else if(!ocname.equals("") && !ocaddress.equals("")) {

                   if(myDb.getcusomerbymob(ocmobile).isEmpty()){
                      boolean Isinserted =  myDb.insertCustomer(ocmobile,ocname,ocemail,ocaddress);

                   }

                  SalesAllRcView.setItemViewCacheSize(lstProducts.size());

                  Long orderid = myDb.insertOrder(date, tvtotal.getText().toString());


                  if (orderid == -1) {
                      Toast.makeText(SaleActivity.this, "Somthing wen't worng, bill not generated", Toast.LENGTH_LONG).show();

                  } else {


                      long ordercustid = myDb.insertOrderCustomer(Integer.parseInt(String.valueOf(orderid)), ocmobile, ocname, ocemail, ocaddress);

                      myDb.UpdateOrder(Integer.parseInt(String.valueOf(orderid)), Integer.parseInt(String.valueOf(ordercustid)));
                      for (int i = 0; i < lstProducts.size(); i++) {

                          View view = SalesAllRcView.getChildAt(i);
                          TextView pname = (TextView) view.findViewById(R.id.tvbillno);
                          EditText pq = (EditText) view.findViewById(R.id.edquantity);
                          TextView pt = (TextView) view.findViewById(R.id.amnt);
                          TextView ratetv = (TextView) view.findViewById(R.id.tvAmnt);

                          int qtyy = 0;
                          String qty = pq.getText().toString();
                          String ptotal = pt.getText().toString();
                          String name = pname.getText().toString();
                          String rate = ratetv.getText().toString();
                          Integer Pid = myDb.getProductid(name);

                          qtyy = myDb.getProductsbyid(Pid);
                          //     if (cursor != null){
                          //       while(cursor.moveToNext()){
                          //         qtyy = Integer.parseInt((cursor.getString(cursor.getColumnIndex("QUANTITY"))));
                          //    }
                          //  cursor.close();
                          //}
                          qtyy = qtyy - Integer.parseInt(qty);

                          myDb.insertStock(Pid, date, "-" + qty);
                          myDb.insertOrderitem(Integer.parseInt(String.valueOf(orderid)), name, qty, rate, ptotal);
                      }
                  }
                  lstProducts.clear();
                  SalesAllRcView.setAdapter(salesAdaptor);
                  salesAdaptor.notifyDataSetChanged();
                  // tvtotal.setText("00");

                  ReportActivity pr = new ReportActivity();
                  //   pr.printInvoice();
              }else {
                  Toast.makeText(SaleActivity.this, "Enter Customer Details", Toast.LENGTH_LONG).show();

              }

            }else {
                 Toast.makeText(SaleActivity.this, "Cart is Empty", Toast.LENGTH_LONG).show();

             }
            }
        });

    }

    public static void gettotall() {

        SalesAllRcView.setItemViewCacheSize(lstProducts.size());

        listtotal = new ArrayList<>();

        for (int i = 0; i < lstProducts.size(); i++) {
//            RecyclerView.LayoutManager recyclerView = null;
  //          View view = SalesAllRcView.getChildAt(i);

  //          TextView totamttv = (TextView) view.findViewById(R.id.amnt);
            String total = getValue(i);


            listtotal.add(total);
        }
        double sum = 0;
        for(int i = 0; i < listtotal.size(); i++){
            sum += Double.parseDouble( listtotal.get(i));
            tvtotamt.setText(String.valueOf(sum));


        }
    }


    public void ScanBarcode(View view) {

        //   IntentIntegrator intentIntegrator = new IntentIntegrator(this);
        //  intentIntegrator.initiateScan();
        startActivity(new Intent(getApplicationContext(),BarcodeScanOrderActivity.class));

    }

    @Override
    protected void onResume() {
        super.onResume();



        }

    public static String getValue(int position){
        return   listtotal.get(position);
    }


    public static String parsePickerDate(int Year, int Month, int day) {

        String sYear = String.valueOf(Year);
        String sMonth = String.valueOf((Month));
        String sDay = String.valueOf(day);

        if (sDay.length() == 1)
            sDay = "0" + sDay;

        if (sMonth.length() == 1)
            sMonth = "0" + sMonth;

        if (sDay.length() == 1)
            sDay = "0" + sDay;

        return sYear + "-" + sMonth + "-" + sDay;

    }



    public static boolean validateOrderProducts(List<Products> cart) {


        boolean doesProductsChanged = false;

            for (Products originalProduct : lstProducts) {
                if (!doesProductsChanged) {
                    for (Products cartProduct : cart) {
                        if (originalProduct.getBarcode().equals(cartProduct.getBarcode())) {

                                doesProductsChanged = true;
                                // cart has been changed -> break from inner loop
                                break;

                        } else {
                            doesProductsChanged = false;
                        }
                    }
                } else {
                    // cart is already changed -> break from first loop
                    break;
                }
            }

        return doesProductsChanged;
    }



}
