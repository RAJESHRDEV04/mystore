package com.example.mystore;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;


import com.example.mystore.adapter.ProductsAdapter;
import com.example.mystore.adapter.ViewPagerAdapter;
import com.example.mystore.db.DbHelper;
import com.example.mystore.fragment.FragmentProducts;
import com.example.mystore.fragment.FragmentSearchProducts;
import com.example.mystore.model.Bill;
import com.example.mystore.model.Products;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProductsActivity extends AppCompatActivity {


    private RecyclerView ProductsAllRcView;
    private List<Products> lstProducts;
    DbHelper myDb;

    ProductsAdapter productsAdapter;

    public Spinner catspin,brandspin;
    FloatingActionButton addfloatbtn;
    ImageView btnhome;
    Button btnrefresh;
    EditText atvname;
    private String selectcat,selectbrand;
    List<String> Bnames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        btnrefresh = findViewById(R.id.btnrefresh);
        atvname = findViewById(R.id.atvname);
        ProductsAllRcView = findViewById(R.id.productsrcview);




        lstProducts = new ArrayList<Products>();
        myDb = new DbHelper(this);
        lstProducts = myDb.getProductsbysearch();
    //    productsAdapter = new ProductsAdapter(this,lstProducts);
        ProductsAllRcView.setLayoutManager(new GridLayoutManager(this,2));


        addfloatbtn=findViewById(R.id.addfloatbtn);
        myDb = new DbHelper(this);

        addfloatbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductsActivity.this,AddproductsActivity.class);
                startActivity(intent);
                finish();
            }
        });
        btnhome=findViewById(R.id.btnhome);
        btnhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductsActivity.this,MainActivity.class);
                startActivity(intent);

            }
        });

        btnrefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                refreshactivity();

                // overridePendingTransition(0, 0);
            }
        });


        atvname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s!=null){
                    filter(s.toString());
                }

            }
        });

        final List<String> Cats = new ArrayList<>();
        Cats.add("All");

        Cursor cursor1 = myDb.getCategories();
        if (cursor1 != null){
            while(cursor1.moveToNext()){
                Cats.add(cursor1.getString(cursor1.getColumnIndex("CATEGORIESNAME")));
            }
            cursor1.close();
        }

        catspin = (Spinner) findViewById(R.id.spinrcategory);
        ArrayAdapter<String> catad = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, Cats);
        catad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        catspin.setAdapter(catad);
        selectcat  = catspin.getSelectedItem().toString();

        catspin.setOnItemSelectedListener(new CategoriesClass());


        Bnames = new ArrayList<>();
        setSelectbrand();


        brandspin  = (Spinner) findViewById(R.id.spinrfilter);
       ArrayAdapter<String> branad = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, Bnames);
       branad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
       brandspin.setAdapter(branad);
       selectbrand  = brandspin.getSelectedItem().toString();

        brandspin.setOnItemSelectedListener(new BrandClass());
        filtterproducts();
    }

    public void refreshactivity() {
        finish();
        overridePendingTransition(0, 0);
        startActivity(getIntent());
    }


    class CategoriesClass implements AdapterView.OnItemSelectedListener
    {
        private String selb,selc;

        public void onItemSelected(AdapterView<?> parent, View v, int position, long id)
        {
            selectcat  = catspin.getSelectedItem().toString();
          //  Bnames.clear();
           // Bnames.add("All");

            setSelectbrand();
           filtterproducts();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }


    }

    class BrandClass implements AdapterView.OnItemSelectedListener
    {
        public void onItemSelected(AdapterView<?> parent, View v, int position, long id)


        {

           selectbrand  = brandspin.getSelectedItem().toString();
            filtterproducts();

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }



    void filtterproducts(){

     //   lstProducts = myDb.getProductsbysearch();



        if(selectcat.equals("All") && selectbrand.equals("All")){
         //   viewPagerAdapter.AddFragment(new FragmentSearchProducts(),"lstProducts");
            lstProducts.clear();
            lstProducts = myDb.getProductsbysearch();
        }else {


            String selectcat = null,selectbr = null;



            selectcat = catspin.getSelectedItem().toString();
            selectbr = brandspin.getSelectedItem().toString();

            if (!selectcat.equals("All") && selectbr.equals("All")) {

                lstProducts.clear();
                lstProducts = myDb.getProductsbysearch(selectcat);
            }else if(selectcat.equals("All") && !selectbr.equals("All")){
                lstProducts.clear();
                lstProducts = myDb.getProductsbybrand(selectbr);
               // ProductsAllRcView.setAdapter(productsAdapter);
                //productsAdapter.notifyDataSetChanged();
            }

           // lstProducts = myDb.getProductsbysearch();
            ProductsAdapter productsAdaptor = new ProductsAdapter(this,lstProducts);

        }
        productsAdapter = new ProductsAdapter(this,lstProducts);
        ProductsAllRcView.setAdapter(productsAdapter);
        productsAdapter.notifyDataSetChanged();


      //  viewPager.setAdapter(viewPagerAdapter);
    }


    public void setSelectbrand(){


        Bnames.clear();
        Bnames.add("All");
        selectcat = catspin.getSelectedItem().toString();
        if(selectcat.equals("All"))
        {

            Cursor cursor = myDb.getBrandName();
            if (cursor != null){
                while(cursor.moveToNext()){
                    Bnames.add(cursor.getString(cursor.getColumnIndex("BRANDNAME")));
                }
                cursor.close();
            }
        }else{
            Cursor cursor = myDb.getBrandName(selectcat);
            if (cursor != null){

                while(cursor.moveToNext()){
                    Bnames.add(cursor.getString(cursor.getColumnIndex("BRANDNAME")));
                }
                cursor.close();
            }
        }


    }


    private void filter(String text) {
        int total = 0;
        ArrayList<Products> filteredList = new ArrayList<>();
        if(!text.isEmpty()){
            for (Products item : lstProducts) {
                if (item.getProductname().toLowerCase().contains(text.toLowerCase())) {
                    filteredList.add(item);
                }
            }
            productsAdapter.filterList(filteredList);





        }else{
            productsAdapter.filterList((ArrayList<Products>) lstProducts);
        }


    }
}




