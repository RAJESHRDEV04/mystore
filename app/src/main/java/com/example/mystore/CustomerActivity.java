package com.example.mystore;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.example.mystore.adapter.CustomerAdapter;
import com.example.mystore.adapter.ProductsAdapter;
import com.example.mystore.db.DbHelper;
import com.example.mystore.model.Customer;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class CustomerActivity extends AppCompatActivity {


    private RecyclerView CustomerAllRcView;
    private List<Customer> lstContact;
    DbHelper myDb;
    CustomerAdapter customerAdapter;
    FloatingActionButton addbtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);



        addbtn = findViewById(R.id.btnadd);

        lstContact = new ArrayList<Customer>();
        myDb = new DbHelper(this);
        lstContact = myDb.getallCustomer();

        CustomerAllRcView = (RecyclerView) findViewById(R.id.rccon);
        CustomerAllRcView.setHasFixedSize(true);

        customerAdapter = new CustomerAdapter(this, lstContact);
        CustomerAllRcView.setLayoutManager(new LinearLayoutManager(this));
        CustomerAllRcView.setAdapter(customerAdapter);

        EditText searchItem = findViewById(R.id.edsrch);


        searchItem.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }});


        searchItem.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });

        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CustomerActivity.this, AddCustomersActivity.class);
                finish();
                startActivity(intent);
            }
        });
    }



    private void filter(String text) {
        ArrayList<Customer> filteredList = new ArrayList<>();
        for (Customer item : lstContact) {
            if (item.getCustomername().toLowerCase().contains(text.toLowerCase())) {
                filteredList.add(item);
            }
        }
        customerAdapter.filterList(filteredList);
    }
}